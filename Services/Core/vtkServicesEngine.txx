/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkServicesEngine.txx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkServicesEngine_txx
#define vtkServicesEngine_txx

#include <chrono>

//-----------------------------------------------------------------------------
template <typename Rep, typename Period>
inline void vtkServicesEngine::ProcessEventsFor(const std::chrono::duration<Rep, Period>& duration)
{
  const auto start = std::chrono::system_clock::now();
  do
  {
    this->ProcessEvents();
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  } while (duration > (std::chrono::system_clock::now() - start));
}

//-----------------------------------------------------------------------------
template <typename Rep, typename Period, typename Predicate>
inline bool vtkServicesEngine::ProcessEventsFor(
  Predicate stopProcessing, const std::chrono::duration<Rep, Period>& duration)
{
  const auto start = std::chrono::system_clock::now();
  do
  {
    this->ProcessEvents();
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  } while (!stopProcessing() && (duration > (std::chrono::system_clock::now() - start)));
  return stopProcessing();
}

//-----------------------------------------------------------------------------
template <typename Predicate>
inline bool vtkServicesEngine::ProcessEventsFor(
  Predicate stopProcessing, const std::chrono::milliseconds& duration)
{
  return this->ProcessEventsFor<int64_t, std::milli, Predicate>(stopProcessing, duration);
}

//-----------------------------------------------------------------------------
template <typename T>
inline T vtkServicesEngine::Await(rxcpp::observable<T> observable)
{
  T returnValue;
  std::atomic<bool> done{ false };
  auto subscription = observable.subscribe([&returnValue, &done](T item) {
    returnValue = item;
    done = true;
  });
  do
  {
    this->ProcessEvents();
  } while (!done.load());
  subscription.unsubscribe();
  return returnValue;
}

//-----------------------------------------------------------------------------
template <typename T>
inline T vtkServicesEngine::Await(
  const rxcpp::schedulers::run_loop& rlp, rxcpp::observable<T> observable)
{
  T returnValue;
  std::atomic<bool> done{ false };
  auto subscription = observable.subscribe([&returnValue, &done](T item) {
    returnValue = item;
    done = true;
  });
  do
  {
    while ((!rlp.empty() && rlp.peek().when < rlp.now()))
    {
      rlp.dispatch();
    }
  } while (!done.load());
  subscription.unsubscribe();
  return returnValue;
}
#endif
