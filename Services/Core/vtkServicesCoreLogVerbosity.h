/*=========================================================================

  Program:   ParaView
  Module:    vtkServicesCoreLogVerbosity.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkServicesCoreLogVerbosity_h
#define vtkServicesCoreLogVerbosity_h

#include "vtkLogger.h"
#include "vtkServicesCoreModule.h"

/**
 * Looks up system environment for `VTKSERVICES_CORE_LOGGER_VERBOSITY` that shall
 * be used to set logger verbosity for the `vtkServicesCoreModule`.
 * The default value is TRACE.
 *
 * Accepted string values are OFF, ERROR, WARNING, INFO, TRACE, MAX, INVALID or ASCII
 * representation for an integer in the range [-9,9].
 *
 * @note This class internally uses vtkLogger::ConvertToVerbosity(const char*).
 */
class VTKSERVICESCORE_EXPORT vtkServicesCoreLogVerbosity
{
public:
  /**
   * Get the current log verbosity for the ServicesCore module.
   */
  static vtkLogger::Verbosity GetVerbosity();
  static void SetVerbosity(vtkLogger::Verbosity verbosity);

  /**
   * Get the current log verbosity for vtkProvider implmentations.
   */
  static vtkLogger::Verbosity GetProviderVerbosity();
  static void SetProviderVerbosity(vtkLogger::Verbosity verbosity);
};

/**
 * Macro to use for verbosity when logging ParaView::ServicesCore messages. Same as calling
 * vtkServicesCoreLogVerbosity::GetVerbosity() e.g.
 *
 * @code{cpp}
 *  vtkVLogF(VTKSERVICESCORE_LOG_VERBOSITY(), "a message");
 *  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "a message");
 * @endcode
 */
#define VTKSERVICESCORE_LOG_VERBOSITY() vtkServicesCoreLogVerbosity::GetVerbosity()
#define VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY() vtkServicesCoreLogVerbosity::GetProviderVerbosity()

#endif // vtkServicesCoreLogVerbosity_h
