/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkRemotingCoreUtilities.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkRemotingCoreUtilities
 * @brief Utility functions used throughout the library for
 *        sanity checks and task execution with threading.
 *
 */

#ifndef vtkRemotingCoreUtilities_h
#define vtkRemotingCoreUtilities_h

#include "vtkObject.h"
#include "vtkServicesCoreModule.h" // for exports

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

#include <thread> // for std::thread

class VTKSERVICESCORE_EXPORT vtkRemotingCoreUtilities : public vtkObject
{
public:
  static vtkRemotingCoreUtilities* New();
  vtkTypeMacro(vtkRemotingCoreUtilities, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  vtkRemotingCoreUtilities(const vtkRemotingCoreUtilities&) = delete;
  void operator=(const vtkRemotingCoreUtilities&) = delete;

  /**
   * Use this in non-threadsafe methods to ensure they are called on the owner
   * thread alone.
   */
  static void EnsureThread(const std::thread::id& owner);

  /**
   * Run a function with arguments on another thread and wait for completion.
   * Beware, if invoked on the same thread as the coordination, it results in a deadlock.
   * Note: If the run loop of `coordination` is busy, it may take a while for this function to
   * return.
   */
  template <typename Function, typename... Args>
  static auto RunAsBlocking(
    Function&& f, rxcpp::observe_on_one_worker coordination, Args&&... args);

  /**
   * Runs a class member function as blocking.
   */
  template <typename Function, typename ClassType, typename... Args>
  static auto RunAsBlocking(
    Function&& f, ClassType self, rxcpp::observe_on_one_worker coordination, Args&&... args);

protected:
  vtkRemotingCoreUtilities();
  ~vtkRemotingCoreUtilities() override;
};

#include "vtkRemotingCoreUtilities.txx"
#endif
