/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkRemotingCoreUtilities.txx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkRemotingCoreUtilities_txx
#define vtkRemotingCoreUtilities_txx

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

//----------------------------------------------------------------------------
template <typename Function, typename... Args>
auto vtkRemotingCoreUtilities::RunAsBlocking(
  Function&& f, rxcpp::observe_on_one_worker coordination, Args&&... args)
{
  // Simply using `just(f(args))` will execute `f` immediately. By using `defer`, it is
  // possible to schedule the creation of the observable itself on given coordination.
  return rxcpp::observable<>::defer([&]() { return rxcpp::observable<>::just(f(args...)); })
    .subscribe_on(coordination)
    .as_blocking()
    .first();
}

//----------------------------------------------------------------------------
template <typename Function, typename ClassType, typename... Args>
auto vtkRemotingCoreUtilities::RunAsBlocking(
  Function&& f, ClassType self, rxcpp::observe_on_one_worker coordination, Args&&... args)
{
  // Simply using `just(f(args))` will execute `f` immediately. By using `defer`, it is
  // possible to schedule the creation of the observable itself on given coordination.
  return rxcpp::observable<>::defer(
    [&]() { return rxcpp::observable<>::just((self->*f)(args...)); })
    .subscribe_on(coordination)
    .as_blocking()
    .first();
}

#endif
