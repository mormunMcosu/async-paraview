/*=========================================================================

  Program:   ParaView
  Module:    vtkServicesAsioLogVerbosity.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkServicesAsioLogVerbosity.h"
#include "vtksys/SystemTools.hxx"

namespace
{
// executed when this library is loaded.
vtkLogger::Verbosity GetInitialServicesAsioVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::ServicesCore module.
  const char* VerbosityKey = "VTKSERVICES_ASIO_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    return vtkLogger::ConvertToVerbosity(verbosity_str);
  }
  else
  {
    return vtkLogger::VERBOSITY_TRACE;
  }
}
}

vtkLogger::Verbosity vtkServicesAsioLogVerbosity::ServicesAsioVerbosity =
  ::GetInitialServicesAsioVerbosity();
