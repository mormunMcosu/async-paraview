/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkAsioInternalsEngine.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @class vtkAsioInternalsEngine
 * @brief Internals class used by vtkAsioServicesEngine to accept
 *        tcp connections or connect with a server with tcp sockets.
 */

#ifndef vtkAsioInternalsEngine_h
#define vtkAsioInternalsEngine_h

#include "vtkAsioInternalsConnection.h"
#include "vtkAsioInternalsEngineSideTypes.h"
#include "vtkAsioInternalsRxCommunicator.h"
#include "vtkAsioService.h"
#include "vtkAsioServiceEndpoint.h"

#include <memory>
#include <thread>

class vtkAsioInternalsEngine
{
public:
  vtkAsioInternalsEngine();
  ~vtkAsioInternalsEngine();

  static int GetSideAsInteger(VTKAsioEngineSideType side);

  // asio helpers
  // Resolve a host, port to a list of endpoints based on resolve_flags.
  tcp::resolver::results_type Resolve(
    std::string host, std::string port, tcp::resolver::flags resolve_flags);
  // Starts the processing of socket events.
  void Start();
  // Stops the io_context. Ideally, you do not have to use this.
  void Stop();

  // completion handlers
  // schedules an asynchronous read from the connected socket. side decides which connection is
  // used.
  void HandleAccept(const error_code& ec, int side);
  // schedules an asynchronous read from the connected socket. side decides which connection is
  // used.
  void HandleConnect(const error_code& ec, tcp::endpoint ep, int side);
  // bumps the appropriate communicator's send counter. side can be service or endpoint.
  void HandleWrite(const error_code& ec, int side);
  // bumps the appropriate communicator's recv counter. side can be service or endpoint.
  // schedules an asynchronous read from the connected socket.
  void HandleRead(const error_code& ec, const std::string& message, int side);

  // asio objects
  std::unique_ptr<tcp::acceptor> Acceptor;
  std::unique_ptr<io::io_context> IOContext;

  // vtkAsioInternals objects.
  // One communicator used by all vtkAsioService(s), another used by all vtkAsioServiceEndpoint(s)
  vtkAsioInternalsRxCommunicator Communicators[2];
  // One connection used by all vtkAsioService(s), another by all vtkAsioServiceEndpoint(s)
  std::shared_ptr<vtkAsioInternalsConnection> Connections[2];

private:
  std::unique_ptr<std::thread> Runner;
};

#endif // vtkAsioInternalsEngine_h

// VTK-HeaderTest-Exclude: vtkAsioInternalsEngine.h
