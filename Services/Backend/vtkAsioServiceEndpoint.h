/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioServiceEndpoint.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkAsioServiceEndpoint
 * @brief A concrete implementation of vtkServiceEndpoint that communicates with asio tcp sockets
 *
 */

#ifndef vtkAsioServiceEndpoint_h
#define vtkAsioServiceEndpoint_h

#include "vtkServiceEndpoint.h"

#include "vtkAsioInternalsEngineSideTypes.h" // ivar
#include "vtkServicesBackendModule.h"        // for export macro

class VTKSERVICESBACKEND_EXPORT vtkAsioServiceEndpoint : public vtkServiceEndpoint
{
public:
  vtkTypeMacro(vtkAsioServiceEndpoint, vtkServiceEndpoint);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  static vtkAsioServiceEndpoint* New();

protected:
  vtkAsioServiceEndpoint();
  ~vtkAsioServiceEndpoint() override;

  void InitializeInternal() override;
  rxcpp::observable<bool> ConnectInternal(
    const std::string& serviceName, const std::string& url) override;
  void ShutdownInternal() override;
  void SendMessageInternal(const vtkPacket& packet) const override;
  rxcpp::observable<vtkPacket> SendRequestInternal(const vtkPacket& packet) const override;
  rxcpp::observable<bool> SubscribeInternal(const std::string& channel) override;
  void UnsubscribeInternal(const std::string& channel) override;

private:
  vtkAsioServiceEndpoint(const vtkAsioServiceEndpoint&) = delete;
  void operator=(const vtkAsioServiceEndpoint&) = delete;
  static constexpr VTKAsioEngineSideType TAG = VTKAsioEngineSideType::VTKAEST_EndpointSide;

  rxcpp::composite_subscription ChannelDispatchPipeline;
};

#endif // vtkAsioServiceEndpoint_h
