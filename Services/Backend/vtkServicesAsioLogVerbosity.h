/*=========================================================================

  Program:   ParaView
  Module:    vtkServicesAsioLogVerbosity.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkServicesAsioLogVerbosity_h
#define vtkServicesAsioLogVerbosity_h

#include "vtkLogger.h"
#include "vtkServicesBackendModule.h"

/**
 * Looks up system environment for `VTKSERVICES_ASIO_LOGGER_VERBOSITY` that shall
 * be used to set logger verbosity for the `vtkServicesBackend`.
 * The default value is TRACE.
 *
 * Accepted string values are OFF, ERROR, WARNING, INFO, TRACE, MAX, INVALID or ASCII
 * representation for an integer in the range [-9,9].
 *
 * @note This class internally uses vtkLogger::ConvertToVerbosity(const char*).
 */
class VTKSERVICESBACKEND_EXPORT vtkServicesAsioLogVerbosity
{
public:
  /**
   * Get the current log verbosity for the ServicesAsio module.
   */
  static vtkLogger::Verbosity GetVerbosity() { return ServicesAsioVerbosity; }

private:
  static vtkLogger::Verbosity ServicesAsioVerbosity;
};

/**
 * Macro to use for verbosity when logging asio trace messages. Same as calling
 * vtkServicesAsioLogVerbosity::GetVerbosity() e.g.
 *
 * @code{cpp}
 *  vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "read header");
 * @endcode
 */
#define VTKSERVICESASIO_LOG_VERBOSITY() vtkServicesAsioLogVerbosity::GetVerbosity()

#endif // vtkServicesAsioLogVerbosity_h
