/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioServicesEngine.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioServicesEngine.h"
#include "vtkAsioInternalsEngine.h"
#include "vtkAsioInternalsNetworkPacket.h"
#include "vtkObjectFactory.h"
#include "vtkServicesAsioLogVerbosity.h"
#include "vtksys/RegularExpression.hxx"

#include <cstdlib> // for atoi

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkAsioServicesEngine);

//----------------------------------------------------------------------------
vtkAsioServicesEngine::vtkAsioServicesEngine() = default;

//----------------------------------------------------------------------------
vtkAsioServicesEngine::~vtkAsioServicesEngine() = default;

//----------------------------------------------------------------------------
void vtkAsioServicesEngine::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
std::string vtkAsioServicesEngine::GetProtocol()
{
  return "tcp";
}

//----------------------------------------------------------------------------
std::string vtkAsioServicesEngine::GetBuiltinProtocol()
{
  return "builtin+tcp";
}

//----------------------------------------------------------------------------
vtkAsioInternalsRxCommunicator* vtkAsioServicesEngine::GetCommunicator(VTKAsioEngineSideType side)
{
  return &(this->Internals->Communicators[this->Internals->GetSideAsInteger(side)]);
}

//----------------------------------------------------------------------------
vtkAsioInternalsConnection* vtkAsioServicesEngine::GetConnection(VTKAsioEngineSideType side)
{
  return this->Internals->Connections[this->Internals->GetSideAsInteger(side)].get();
}

//----------------------------------------------------------------------------
std::string vtkAsioServicesEngine::ExtractHost(const std::string& url)
{
  vtksys::RegularExpression regEx("^[^:]+://(.*):([0-9]+)$");
  if (!regEx.find(url))
  {
    vtkLog(ERROR, "Invalid url - " << url);
    return {};
  }
  return regEx.match(1);
}

//----------------------------------------------------------------------------
std::string vtkAsioServicesEngine::ExtractPort(const std::string& url)
{
  vtksys::RegularExpression regEx("^[^:]+://(.*):([0-9]+)$");
  if (!regEx.find(url))
  {
    vtkLog(ERROR, "Invalid url - " << url);
    return {};
  }
  return regEx.match(2);
}

//----------------------------------------------------------------------------
std::string vtkAsioServicesEngine::InitializeInternal(const std::string& url)
{
  this->Internals.reset(new vtkAsioInternalsEngine());
  auto& internals = (*this->Internals);
  std::string host;
  std::string port;

  if (url.find("builtin+tcp") != std::string::npos)
  {
    // builtin session
    host = "";
    port = "0";
  }
  else if (url == "tcp://")
  {
    // remote session, here as a client
    return url;
  }
  else
  {
    // remote session, here as a server.
    host = vtkAsioServicesEngine::ExtractHost(url);
    port = vtkAsioServicesEngine::ExtractPort(url);
  }
  auto resolvedUrl = this->Accept(host, port);
  // start the asio::io_context event loop
  internals.Start();
  return resolvedUrl;
}

//----------------------------------------------------------------------------
std::string vtkAsioServicesEngine::Accept(const std::string& host, const std::string& port)
{
  auto& internals = (*this->Internals);
  auto& ioCtx = (*internals.IOContext);
  std::string address;
  asio::ip::port_type portNumber = 0;
  error_code ec;

  // if host is already an ip address, indicate that host is a numeric string
  auto ipAddress = asio::ip::make_address(host, ec);
  tcp::resolver::results_type endpoints;
  if (!ec)
  {
    // start server on specific IP address
    vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Valid IP address %s", host.c_str());
    internals.Acceptor.reset(
      new tcp::acceptor(ioCtx, tcp::endpoint(ipAddress, std::atoi(port.c_str()))));
    address = internals.Acceptor->local_endpoint().address().to_string();
    portNumber = internals.Acceptor->local_endpoint().port();
  }
  else if (!host.empty())
  {
    // start accepting on any ip
    internals.Acceptor.reset(
      new tcp::acceptor(ioCtx, tcp::endpoint(tcp::v6(), std::atoi(port.c_str()))));
    for (auto& epIter : internals.Resolve(host, port, tcp::resolver::flags()))
    {
      auto ep = epIter.endpoint();
      vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Available endpoint: %s:%d",
        ep.address().to_string().c_str(), ep.port());
    }
    address = internals.Acceptor->local_endpoint().address().to_string();
    portNumber = internals.Acceptor->local_endpoint().port();
  }
  else
  {
    // start builtin server.
    endpoints = internals.Resolve(host, port, tcp::resolver::flags());
    internals.Acceptor = std::unique_ptr<tcp::acceptor>(new tcp::acceptor(ioCtx));
    // find a working endpoint i.e, (addr, port) which an acceptor can use.
    for (auto& epIter : endpoints)
    {
      auto ep = epIter.endpoint();
      vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Trying %s ..", ep.address().to_string().c_str());

      internals.Acceptor->open(epIter.endpoint().protocol(), ec);
      if (ec)
      {
        vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Cannot open %s (%s)",
          ep.address().to_string().c_str(), ec.message().c_str());
        internals.Acceptor->close();
        continue;
      }

      internals.Acceptor->set_option(asio::ip::tcp::acceptor::reuse_address(true));
      internals.Acceptor->bind(epIter.endpoint(), ec);
      if (ec)
      {
        vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Cannot bind %s (%s)",
          ep.address().to_string().c_str(), ec.message().c_str());
        internals.Acceptor->close();
        continue;
      }
      auto localEp = internals.Acceptor->local_endpoint();
      address = localEp.address().to_string();
      portNumber = localEp.port();
      break;
    }
    // Acceptor failed to bind a socket on host:port
    if (address.empty())
    {
      vtkLogF(ERROR, "Failed to resolve %s:%s. Error (%d) %s", host.c_str(), port.c_str(),
        ec.value(), ec.message().c_str());
      std::terminate();
    }
    // listen for incoming connections.
    internals.Acceptor->listen(tcp::socket::max_listen_connections, ec);
    if (ec)
    {
      vtkLogF(ERROR, "Failed to listen %s:%d. Error code (%d) %s", address.c_str(), portNumber,
        ec.value(), ec.message().c_str());
      std::terminate();
    }
  }

  vtkVLogF(
    VTKSERVICESASIO_LOG_VERBOSITY(), "Accepting connections %s:%d", address.c_str(), portNumber);

  // schedule an accept.
  const int side = internals.GetSideAsInteger(VTKAsioEngineSideType::VTKAEST_ServiceSide);
  internals.Connections[side] = std::make_shared<vtkAsioInternalsConnection>(ioCtx);
  internals.Acceptor->async_accept(internals.Connections[side]->GetSocket(),
    [this, &internals, side, host, port](auto&& PH1) { internals.HandleAccept(PH1, side); });

  return this->GetProtocol() + "://" + (host.empty() ? address : host) + ":" +
    std::to_string(portNumber);
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkAsioServicesEngine::Connect(
  const std::string& host, const std::string& port)
{
  auto& internals = (*this->Internals);
  auto& ioCtx = (*internals.IOContext);
  const int side = internals.GetSideAsInteger(VTKAsioEngineSideType::VTKAEST_EndpointSide);
  internals.Connections[side] = std::make_shared<vtkAsioInternalsConnection>(ioCtx);

  // cold observable
  auto source = rxcpp::observable<>::create<bool>(
    [&internals, side, host, port](const rxcpp::subscriber<bool>& subscriber) {
      auto conn = internals.Connections[side];
      auto endpoints = internals.Resolve(host, port, tcp::resolver::resolver_base::flags());
      for (auto& epIter : internals.Resolve(host, port, tcp::resolver::flags()))
      {
        auto ep = epIter.endpoint();
        vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Available endpoint: %s:%d",
          ep.address().to_string().c_str(), ep.port());
      }
      // schedule a non-blocking connect
      asio::async_connect(conn->GetSocket(), endpoints,
        [&internals, side, subscriber](const error_code& ec, tcp::endpoint ep) {
          internals.HandleConnect(ec, ep, side);
          subscriber.on_next(ec.value() == 0);
        });
    });
  // start the asio::io_context event loop
  internals.Start();
  return source.take(1);
}

//----------------------------------------------------------------------------
void vtkAsioServicesEngine::FinalizeInternal()
{
  this->Internals.reset();
}
