/*=========================================================================

  Program:   Visualization Toolkit
  Module:    TestUncachedChannel.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <vtkChannelSubscription.h>
#include <vtkLogger.h>
#include <vtkNew.h>
#include <vtkPacket.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#include <chrono>
#include <thread>

#define VERIFY(x)                                                                                  \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Failed " #x);                                                                  \
    std::terminate();                                                                              \
  }

namespace
{
bool MakeRequestToPublish(const std::string& channel, int messageId, vtkServiceEndpoint* endpoint)
{
  auto engine = endpoint->GetEngine();
  engine->Await(
    endpoint->SendRequest(vtkPacket({ { "channel", channel }, { "messageId", messageId } })));
  return true;
}
}

int TestUncachedChannel(int /*argc*/, char* /*argv*/[])
{
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());

  auto service = engine->CreateService("service1");
  service->GetRequestObservable().subscribe([](const vtkServiceReceiver& receiver) {
    /* every time we receive a request, we publish the same message */
    const auto& packet = receiver.GetPacket();
    const auto channel = packet.GetJSON().at("channel").get<std::string>();
    receiver.Publish(channel, packet);
    receiver.Respond(packet);
  });

  VERIFY(service->Start());

  auto endpoint = engine->CreateServiceEndpoint("service1");
  VERIFY(engine->Await(endpoint->Connect()));
  vtkLogScopeF(INFO, "TestUncachedChannel");
  // Make service publish messages before subscription is made.
  // They will be lost.
  MakeRequestToPublish("channel-uncached", 100, endpoint);
  MakeRequestToPublish("channel-uncached", 101, endpoint);
  MakeRequestToPublish("channel-uncached", 102, endpoint);

  std::vector<int> receivedIds;
  auto subscription = endpoint->Subscribe("channel-uncached");
  subscription->GetObservable().subscribe([&](const vtkPacket& packet) {
    receivedIds.push_back(packet.GetJSON().at("messageId").get<int>());
  });
  endpoint->GetEngine()->Await(subscription->IsReady());

  MakeRequestToPublish("channel-uncached", 201, endpoint);
  MakeRequestToPublish("channel-uncached", 202, endpoint);
  MakeRequestToPublish("channel-uncached", 203, endpoint);

  // Now unsubscribe.
  subscription = nullptr;

  // Now publish a few more messages that should be lost.
  MakeRequestToPublish("channel-uncached", 301, endpoint);
  MakeRequestToPublish("channel-uncached", 302, endpoint);
  MakeRequestToPublish("channel-uncached", 303, endpoint);

  endpoint->GetEngine()->ProcessEventsFor(std::chrono::milliseconds(10));
  /// vtkLogF(INF0,"%d ",receivedIds.size());
  for (int x : receivedIds)
  {
    vtkLogF(INFO, "%d", x);
  }
  VERIFY(receivedIds == std::vector<int>({ 201, 202, 203 }));

  engine->Finalize();
  return EXIT_SUCCESS;
}
