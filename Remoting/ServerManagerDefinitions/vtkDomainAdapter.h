/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkDomainAdapter.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkDomainAdapter_h
#define vtkDomainAdapter_h
#include "vtkObject.h"
#include "vtkRemotingServerManagerDefinitionsModule.h"
#include "vtkSmartPointer.h"
#include <string>

class vtkSMDomain;
class vtkProxyAdapter;
class vtkSMDocumentation;

class VTKREMOTINGSERVERMANAGERDEFINITIONS_EXPORT vtkDomainAdapter : public vtkObject
{
public:
  static vtkDomainAdapter* New();
  vtkTypeMacro(vtkDomainAdapter, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  std::string GetName() const;
  std::string GetClassName() const;

protected:
  vtkDomainAdapter();
  ~vtkDomainAdapter() override;

  //@{
  friend class vtkPropertyAdapter;
  void SetSMDomain(vtkSMDomain* domain);
  //@}

private:
  vtkSmartPointer<vtkSMDomain> SMDomain;

  vtkDomainAdapter(const vtkDomainAdapter&) = delete;
  void operator=(const vtkDomainAdapter&) = delete;
};
#endif
