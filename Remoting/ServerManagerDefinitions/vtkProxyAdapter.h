/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkProxyAdapter.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkProxyAdapter_h
#define vtkProxyAdapter_h
#include "vtkRemotingServerManagerDefinitionsModule.h"

#include "vtkObject.h"
#include "vtkSmartPointer.h"

#include <string>
#include <vector>

class vtkSMProxy;
class vtkPropertyAdapter;
class vtkPropertyGroupAdapter;

/**
 * @brief Definition model of a proxy.
 *
 * vtkProxyAdapter is an adapter of the vtkSMProxy that exposes only the
 * information required to create a proxy definition for the UI such as property
 * types and domains types. This is a read-only adadpter.
 */
class VTKREMOTINGSERVERMANAGERDEFINITIONS_EXPORT vtkProxyAdapter : public vtkObject
{
public:
  static vtkProxyAdapter* New();
  vtkTypeMacro(vtkProxyAdapter, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  // Properties appear in the same order as in the XML definition
  std::vector<vtkSmartPointer<vtkPropertyAdapter>> GetProperties() const;
  // Groups appear in the same order as in the XML definition
  std::vector<vtkSmartPointer<vtkPropertyGroupAdapter>> GetPropertyGroups() const;

  std::string GetName() const;
  std::string GetGroup() const;

  // move assigment operator. After this operation `other`s members are invalid.
  virtual void Move(vtkProxyAdapter* other);

protected:
  vtkProxyAdapter();
  ~vtkProxyAdapter() override;

  //@{
  friend class vtkDefinitionManagerMicroservice;
  //@note  if a proxy is not  available use vtkProxyDefinitionManager to get a
  // prototype based on group and name
  void SetSMProxy(vtkSMProxy* proxy);
  //@}

private:
  vtkProxyAdapter(const vtkProxyAdapter&) = delete;
  void operator=(const vtkProxyAdapter&) = delete;

  std::vector<vtkSmartPointer<vtkPropertyAdapter>> Properties;
  std::vector<vtkSmartPointer<vtkPropertyGroupAdapter>> PropertyGroups;
  vtkSmartPointer<vtkSMProxy> SMProxy;
};
#endif
