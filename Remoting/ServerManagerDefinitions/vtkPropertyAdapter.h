/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPropertyAdapter.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkPropertyAdapter_h
#define vtkPropertyAdapter_h
#include "vtkRemotingServerManagerDefinitionsModule.h"

#include "vtkObject.h"
#include "vtkSmartPointer.h"
#include <string>
#include <vector>

class vtkSMProperty;
class vtkProxyAdapter;
class vtkSMDocumentation;
class vtkDomainAdapter;
class vtkPVXMLElement;
class vtkPropertyGroupAdapter;

class VTKREMOTINGSERVERMANAGERDEFINITIONS_EXPORT vtkWidgetAdapter
{
public:
  std::string GetWidgetClassName() const { return PanelWidget; }

  std::string GetWidgetLayout() const { return Layout; }

  vtkWidgetAdapter() = default;
  vtkWidgetAdapter(const std::string& panel, const std::string& layout)
  {
    this->PanelWidget = panel;
    this->Layout = layout;
  }
  std::string PanelWidget;
  std::string Layout;
};
class VTKREMOTINGSERVERMANAGERDEFINITIONS_EXPORT vtkPropertyAdapter : public vtkObject
{
public:
  static vtkPropertyAdapter* New();
  vtkTypeMacro(vtkPropertyAdapter, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  std::string GetName() const;
  std::string GetLabel() const;
  vtkSMDocumentation* GetDocumentation() const;
  std::string GetDocumentationDescriptionAsString() const;
  bool GetInformationOnly() const;
  bool GetIsInternal() const;
  bool GetIsRepeatable() const;

  int GetSize() const;
  int GetType() const;

  std::string GetPanelVisibility() const;
  vtkWidgetAdapter GetPanelWidget() const;
  std::string GetPropertyClassName() const;
  vtkSmartPointer<vtkPVXMLElement> GetHints() const;

  std::vector<vtkSmartPointer<vtkDomainAdapter>> GetDomains() const;

  vtkSmartPointer<vtkPropertyGroupAdapter> GetPropertyGroup() const;

  std::vector<std::string> GetDecorators() const;

protected:
  vtkPropertyAdapter();
  ~vtkPropertyAdapter() override;

  //@{
  friend class vtkProxyAdapter;
  void SetSMProperty(vtkSMProperty* property);
  void SetPropertyGroup(vtkSmartPointer<vtkPropertyGroupAdapter> group);
  //@}
  void PopulateWidget();

private:
  vtkSmartPointer<vtkSMProperty> SMProperty;
  vtkSmartPointer<vtkPropertyGroupAdapter> Group;
  std::vector<vtkSmartPointer<vtkDomainAdapter>> Domains;
  vtkWidgetAdapter Widget;

  vtkPropertyAdapter(const vtkPropertyAdapter&) = delete;
  void operator=(const vtkPropertyAdapter&) = delete;
};
#endif
