/*=========================================================================

  Program:   ParaView
  Module:    vtkRemoteFileSystemProvider.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkRemoteFileSystemProvider
 * @brief provider that adds support browsing and modifying a remote filesystem
 *
 * @sa vtkLocalFileSystemProvider
 */

#ifndef vtkRemoteFileSystemProvider_h
#define vtkRemoteFileSystemProvider_h
#include "vtkRemotingServerManagerCoreModule.h" // for exports

#include "vtkPVFileInformation.h"
#include "vtkPacket.h" // for vtkPacket
#include "vtkProvider.h"

class VTKREMOTINGSERVERMANAGERCORE_EXPORT vtkRemoteFileSystemProvider : public vtkProvider
{
public:
  static vtkRemoteFileSystemProvider* New();
  vtkTypeMacro(vtkRemoteFileSystemProvider, vtkProvider);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * These methods can be used to construct a message to send to provider.
   */
  ///@{
  static vtkPacket MakeDirectory(const std::string& fullpath);
  static vtkPacket Remove(const std::string& fullpath);
  static vtkPacket Rename(const std::string& oldFullPath, const std::string& newFullPath);
  static vtkPacket ListDirectory(vtkPVFileInformation* information);
  static vtkPacket FileExists(const std::string& fullPath);
  ///@}

  static bool ParseResponse(const vtkNJson& state, vtkPVFileInformation* information);
  static bool ParseResponse(const vtkPacket& packet, vtkPVFileInformation* information);

protected:
  vtkRemoteFileSystemProvider();
  ~vtkRemoteFileSystemProvider() override;

  void InitializeInternal(vtkService* service) override;

  /**
   * Preview a message. Called on the RPC thread.
   */
  virtual void Preview(const vtkPacket& packet);

  ///@{
  /**
   * Process a message. Called on the RPC thread if "run_on_rpc" is true,
   * otherwise called on the service's main thread.
   */
  virtual vtkPacket ProcessOnRPC(const vtkPacket& packet);
  virtual vtkPacket Process(const vtkPacket& packet);
  ///@}

private:
  vtkRemoteFileSystemProvider(const vtkRemoteFileSystemProvider&) = delete;
  void operator=(const vtkRemoteFileSystemProvider&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
