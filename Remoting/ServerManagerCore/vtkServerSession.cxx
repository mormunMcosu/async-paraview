/*=========================================================================

  Program:   ParaView
  Module:    vtkServerSession.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkServerSession.h"

#include "vtkObjectFactory.h"
#include "vtkObjectStore.h"
#include "vtkPVCoreApplication.h"
#include "vtkPVServerApplication.h"
#include "vtkRemoteFileSystemProvider.h"
#include "vtkRemoteObjectProvider.h"
#include "vtkService.h"
#include "vtkServicesEngine.h"

struct ServiceInfo
{
  vtkSmartPointer<vtkService> Service;
  vtkSmartPointer<vtkObjectStore> ObjectStore;
  std::vector<vtkSmartPointer<vtkProvider>> Providers;
};

class vtkServerSession::vtkInternals
{
  std::map<std::string, ServiceInfo> Services;
  std::vector<rxcpp::observable<vtkServiceReceiver>> ExitObservables;
  rxcpp::composite_subscription ExitSubscription;
  std::set<int> ServiceExitCodes;

public:
  void StartService(const std::string& name, vtkServerSession* self);
  void SetupExitHandlers();

  ~vtkInternals()
  {
    this->ExitSubscription.unsubscribe();
    for (auto& pair : this->Services)
    {
      if (pair.second.Service)
      {
        auto* engine = pair.second.Service->GetEngine();
        // Clear the providers
        pair.second.Providers.clear();
        // shutdown so no RPCs will be executed while the service is deallocating itself.
        pair.second.Service->Shutdown();
        engine->UnRegisterService(pair.second.Service);
      }
    }
  }
};

//----------------------------------------------------------------------------
void vtkServerSession::vtkInternals::StartService(const std::string& name, vtkServerSession* self)
{
  auto* pvapp = vtkPVCoreApplication::GetInstance();
  assert(pvapp != nullptr);
  auto* engine = pvapp->GetServicesEngine();

  ServiceInfo info;
  info.ObjectStore = vtk::TakeSmartPointer(vtkObjectStore::New());
  info.Service = engine->CreateService(name);

  auto remoteObjectProvider = vtk::TakeSmartPointer(vtkRemoteObjectProvider::New());
  remoteObjectProvider->SetObjectStore(info.ObjectStore);
  remoteObjectProvider->SetProxyDefinitionManager(self->GetProxyDefinitionManager());
  remoteObjectProvider->Initialize(info.Service);
  info.Providers.push_back(remoteObjectProvider);

  auto remoteFileSystemProvider = vtk::TakeSmartPointer(vtkRemoteFileSystemProvider::New());
  remoteFileSystemProvider->Initialize(info.Service);
  info.Providers.push_back(remoteFileSystemProvider);

  // let sub-classes initialize services.
  self->InitializeService(info.Service, info.ObjectStore);

  this->ExitObservables.emplace_back(
    info.Service
      ->GetRequestObservable(/*skipEventLoop=*/true)
      // look for the special disconnect message. this is guaranteed to be the final
      // message sent by a service from vtkClientSession
      .filter([](const vtkServiceReceiver& receiver) {
        auto& json = receiver.GetPacket().GetJSON();
        auto iter = json.find(vtkServerSession::GetDisconnectTypeString());
        return iter != json.end();
      })
      .take(1));

  this->Services.emplace(name, info);
  info.Service->Start();
}

//----------------------------------------------------------------------------
void vtkServerSession::vtkInternals::SetupExitHandlers()
{
  auto* pvapp = vtkPVCoreApplication::GetInstance();
  assert(pvapp != nullptr);
  auto* engine = pvapp->GetServicesEngine();

  auto values = rxcpp::observable<>::iterate(this->ExitObservables);
  this->ExitSubscription =
    // collect exit codes from all services. this ensures everyone has sent their
    // exit codes before the server application is asked to exit inside `on_completed`
    // handler below.
    values.merge()
      .observe_on(engine->GetCoordination())
      .subscribe(
        // on_next
        [this](const vtkServiceReceiver& receiver) {
          const auto& json = receiver.GetPacket().GetJSON();
          auto exitEnum = json[vtkServerSession::GetDisconnectTypeString()]
                            .get<vtkPVServerApplication::PVServerExitCodeEnum>();
          const int exitCode = static_cast<int>(exitEnum);
          // simply echo the exit code back to the endpoint,
          // to let them know we got it.
          vtkNJson reply = vtkNJson::object();
          reply["exit"] = exitCode;
          receiver.Respond(reply);
          this->ServiceExitCodes.insert(exitCode);
          vtkLogF(TRACE, "Exit code \'%d\' received", static_cast<int>(exitEnum));
        },
        // on_completed
        [this, pvapp]() {
          const auto exitCode = static_cast<int>(*this->ServiceExitCodes.begin());
          pvapp->Exit(exitCode);
        });
}

//============================================================================
vtkObjectFactoryNewMacro(vtkServerSession);
//----------------------------------------------------------------------------
vtkServerSession::vtkServerSession()
  : Internals(new vtkInternals())
{
}

//----------------------------------------------------------------------------
vtkServerSession::~vtkServerSession() = default;

//----------------------------------------------------------------------------
void vtkServerSession::StartServices()
{
  auto& internals = (*this->Internals);
  internals.StartService("ds", this);
  internals.StartService("rs", this);
  internals.SetupExitHandlers();
}

//----------------------------------------------------------------------------
void vtkServerSession::InitializeService(vtkService*, vtkObjectStore*) {}

//----------------------------------------------------------------------------
void vtkServerSession::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
