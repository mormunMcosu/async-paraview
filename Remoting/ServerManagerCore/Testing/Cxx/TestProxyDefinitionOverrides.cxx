/*=========================================================================

  Program: ParaView
  Module:  TestProxyDefinitionOverrides.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkProxyDefinitionManager.h"
#include <sstream>

namespace
{
std::string test_data_0_pet_utilities = R"(<ServerManagerConfiguration>
<ProxyGroup name="PetUtilities">
  <!-- ==================================================================== -->
  <Proxy class="vtkPetWasher"
         label="Pet washer"
         name ="PetWasher">
    <Documentation long_help="Generic utilitiy that washes your pet."
                   short_help="Generic pet washer">
    </Documentation>

    <IntVectorProperty command="SetAge"
                       name="Age"
                       label="Age"
                       number_of_elements="1"
                       default_values="10">
      <Documentation> This property specifies age of your pet. </Documentation>
    </IntVectorProperty>

    <IntVectorProperty command="SetMaxAge"
                       name="MaxAge"
                       label="Max age"
                       number_of_elements="1"
                       default_values="2">
      <Documentation> This property specifies maximum age of your pet. 
        Child proxies can specify a suitable default and override.
      </Documentation>
    </IntVectorProperty>

  </Proxy>
</ProxyGroup>

<ProxyGroup name="FelineUtilities">
  <!-- ==================================================================== -->
  <Proxy class="vtkFelinePetWasher"
         label="Feline pet washer"
         base_proxygroup="PetUtilities"
         base_proxyname="PetWasher"
         name ="FelinePetWasher">
    <Documentation long_help="Utilitiy that washes your feline friend."
                   short_help="feline pet washer">
    </Documentation>

    <IntVectorProperty command="SetMaxAge"
                       name="MaxAge"
                       label="Max age"
                       number_of_elements="1"
                       default_values="20"
                       override="1">
      <Documentation> This property specifies maximum age of your feline friend. 
        Child proxies can specify a suitable default and override.
      </Documentation>
    </IntVectorProperty>

  </Proxy>
</ProxyGroup>

</ServerManagerConfiguration>)";

std::string baseline_data_felineutilities =
  R"(<Proxy class="vtkFelinePetWasher" label="Feline pet washer" name="FelinePetWasher">
  <IntVectorProperty command="SetAge" name="Age" label="Age" number_of_elements="1" default_values="10">
    <Documentation> This property specifies age of your pet. </Documentation>
  </IntVectorProperty>
  <IntVectorProperty command="SetMaxAge" name="MaxAge" label="Max age" number_of_elements="1" default_values="20" override="1">
    <Documentation> This property specifies maximum age of your feline friend. 
        Child proxies can specify a suitable default and override.
      </Documentation>
  </IntVectorProperty>
  <Documentation long_help="Utilitiy that washes your feline friend." short_help="feline pet washer" />
</Proxy>
)";
}

int TestProxyDefinitionOverrides(int, char*[])
{
  vtkNew<vtkProxyDefinitionManager> pxm;
  pxm->LoadConfigurationXML(::test_data_0_pet_utilities);
  auto doc = pxm->FindProxy("FelineUtilities", "FelinePetWasher");
  std::ostringstream ost;
  doc->print(ost, "  ");
  return ost.str() == ::baseline_data_felineutilities ? 0 : 1;
}
