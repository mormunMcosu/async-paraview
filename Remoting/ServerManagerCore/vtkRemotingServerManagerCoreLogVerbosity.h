/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingServerManagerCoreLogVerbosity.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkRemotingServerManagerCoreLogVerbosity_h
#define vtkRemotingServerManagerCoreLogVerbosity_h

#include "vtkLogger.h"
#include "vtkRemotingServerManagerCoreModule.h"

/**
 * Looks up system environment for `VTKREMOTINGSERVERMANAGER_CORE_LOGGER_VERBOSITY` that shall
 * be used to set logger verbosity for the `vtkRemotingServerManagerCoreModule`.
 * The default value is TRACE.
 *
 * Accepted string values are OFF, ERROR, WARNING, INFO, TRACE, MAX, INVALID or ASCII
 * representation for an integer in the range [-9,9].
 *
 * @note This class internally uses vtkLogger::ConvertToVerbosity(const char*).
 */
class VTKREMOTINGSERVERMANAGERCORE_EXPORT vtkRemotingServerManagerCoreLogVerbosity
{
public:
  /**
   * Get the current log verbosity for the RemotingServerManagerCore module.
   */
  static vtkLogger::Verbosity GetVerbosity();
  static void SetVerbosity(vtkLogger::Verbosity verbosity);
  /**
   * Get the current log verbosity for vtkProvider implmentations.
   */
  static vtkLogger::Verbosity GetDataMovementVerbosity();
  static void SetDataMovementVerbosity(vtkLogger::Verbosity verbosity);
};

/**
 * Macro to use for verbosity when logging Paraview::RemotingServerManagerCore messages. Same as
 * calling vtkRemotingServerManagerCoreLogVerbosity::GetVerbosity() e.g.
 *
 * @code{cpp}
 *  vtkVLogF(VTKREMOTINGSERVERMANAGERCORE_LOG_VERBOSITY(), "a message");
 *  vtkVLogF(VTKREMOTINGSERVERMANAGERCORE_DATAMOVEMENT_LOG_VERBOSITY(), "a message");
 * @endcode
 */
#define VTKREMOTINGSERVERMANAGERCORE_LOG_VERBOSITY()                                               \
  vtkRemotingServerManagerCoreLogVerbosity::GetVerbosity()
#define VTKREMOTINGSERVERMANAGERCORE_DATAMOVEMENT_LOG_VERBOSITY()                                  \
  vtkRemotingServerManagerCoreLogVerbosity::GetDataMovementVerbosity()

#endif // vtkRemotingServerManagerCoreLogVerbosity_h
