/*=========================================================================

  Program:   ParaView
  Module:    vtkObjectWrapper.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkObjectWrapper
 * @brief a wrapper for vtkObject
 *
 * vtkObjectWrapper is used to create/update vtkObject-subclass instances on a
 * remote service using the vtkRemoteObjectProvider.
 */

#ifndef vtkObjectWrapper_h
#define vtkObjectWrapper_h

#include "vtkNJsonFwd.h"
#include "vtkObject.h"
#include "vtkRemotingServerManagerCoreModule.h" // for exports
#include "vtkSMPropertyTypes.h"                 // for ivar

#include <memory>        // for std::unique_ptr
#include <vtk_pugixml.h> // for pugi

class vtkRemoteObjectProvider;

class VTKREMOTINGSERVERMANAGERCORE_EXPORT vtkObjectWrapper : public vtkObject
{
public:
  static vtkObjectWrapper* New();
  vtkTypeMacro(vtkObjectWrapper, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Setup the object wrapper by processing property definitions from the XML
   * file.
   */
  virtual bool ReadXMLAttributes(const pugi::xml_node& node);

  /**
   * Initializes the wrapper.
   */
  virtual bool Initialize(
    vtkTypeUInt32 gid, const vtkNJson& state, vtkRemoteObjectProvider* provider);

  /**
   * Returns the global id assigned to this wrapper.
   */
  vtkTypeUInt32 GetGlobalID() const;

  /**
   * Update the wrapped object using the state.
   */
  virtual void UpdateState(const vtkNJson& state, vtkRemoteObjectProvider* provider);

  /**
   * Invokes a command. Command needs to be present in the XML definition of the associated proxy.
   */
  void InvokeCommand(const std::string& command);

  /** Executes a CanReadFile on underlying vtkObject, if that exists
   * Returns 1 if file can be read, 0 if not -1 if the method does not exist for this reader
   */
  int CanReadFile(const std::string& fileName, const std::string& secondItem = {});

  /**
   * Returns state for all information properties.
   */
  vtkNJson UpdateInformation(vtkRemoteObjectProvider* provider) const;

  /**
   * Update pipeline.
   */
  bool UpdatePipeline(double time) const;

  /**
   * Returns the VTK object this wrapper is wrapping.
   *
   * This is created in `ReadXMLAttributes` and hence will not be valid before
   * that method has been called, which happens during initialization stage.
   */
  vtkObject* GetVTKObject() const;

  // Atomically increment the internal counter that keeps track how many times
  // vtkRemoteObjectProvider::Preview() has been applied on the object.
  void IncrementPreviewCounter();

protected:
  vtkObjectWrapper();
  ~vtkObjectWrapper() override;

  // lightweight property information
  struct PropertyInformation
  {
    std::string Method;
    int Type{ vtkSMPropertyTypes::INVALID };
    bool InformationOnly{ false };
  };

  /**
   * Applies proxy properties on the VTK Object.
   */
  virtual bool UpdatePropertyValues(const vtkNJson& value, const PropertyInformation& pinfo,
    vtkSmartPointer<vtkObject> VTKObject, vtkRemoteObjectProvider* provider);

  virtual PropertyInformation TransformPropertyInformation(
    const PropertyInformation& inPropertyInfo)
  {
    return inPropertyInfo;
  }

private:
  vtkObjectWrapper(const vtkObjectWrapper&) = delete;
  void operator=(const vtkObjectWrapper&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
