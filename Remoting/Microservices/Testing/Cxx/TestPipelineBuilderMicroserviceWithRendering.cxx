/*=========================================================================

Program:   ParaView
Module:    TestPipelineBuilderMicroserviceWithRendering.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkPipelineBuilderMicroservice rendering API.
 */
#include "vtkClientSession.h"
#include "vtkDataObject.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkPropertyManagerMicroservice.h"
#include "vtkRegressionTestImage.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMViewProxy.h"
#include "vtkSmartPointer.h"
#include "vtkTestUtilities.h"

#include <cstdlib>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

bool DoPipelineBuilderMicroserviceTestWithRendering(
  int argc, char** argv, vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{

  vtkNew<vtkPipelineBuilderMicroservice> pipelineBuilder;
  vtkNew<vtkPropertyManagerMicroservice> propertyManager;
  auto app = vtkPVApplication::GetInstance();

  pipelineBuilder->SetSession(session);

  // Create source proxy
  auto source = vtk::TakeSmartPointer(
    app->Await(runLoop, pipelineBuilder->CreateSource("sources", "SphereSource")));
  VALIDATE(source != nullptr);

  propertyManager->SetPropertyValue(source, "EndPhi", 90);
  propertyManager->SetPropertyValue(source, "Center", { 1, 2, 3 });

  // Create a filter
  auto filter = vtk::TakeSmartPointer(
    app->Await(runLoop, pipelineBuilder->CreateFilter("filters", "ShrinkFilter", source)));
  VALIDATE(filter != nullptr);

  // Create View
  auto view =
    vtk::TakeSmartPointer(app->Await(runLoop, pipelineBuilder->CreateView("views", "RenderView")));

  // Create a Geometry representation for the output of the filter
  auto representation = vtk::TakeSmartPointer(app->Await(
    runLoop, pipelineBuilder->CreateRepresentation(filter, 0, view, "GeometryRepresentation")));
  VALIDATE(representation != nullptr);

  // update the view
  vtkNew<vtkRenderWindowInteractor> iren;
  iren->SetRenderWindow(view->GetRenderWindow());
  iren->Initialize();

  bool viewUpdated = false;
  view->Update().subscribe([&viewUpdated, view](bool status) {
    view->ResetCameraUsingVisiblePropBounds();
    viewUpdated = status;
  });

  if (vtkRegressionTestImage(view->GetRenderWindow()) == vtkTesting::DO_INTERACTOR)
  {
    while (!iren->GetDone())
    {
      iren->ProcessEvents();
      app->ProcessEvents(runLoop);
    }
  }
  app->ProcessEventsUntil(runLoop, [&viewUpdated]() { return viewUpdated; });

  // representation should not prevent deleting the proxy
  vtkSMSessionProxyManager* pxm = session->GetProxyManager();
  const vtkTypeInt32 representationId = representation->GetGlobalID();
  const vtkTypeInt32 shrinkId = filter->GetGlobalID();
  VALIDATE(app->Await(runLoop, pipelineBuilder->DeleteProxy(filter)));

  // release local references, FindProxy looks into all proxies not just registered
  filter = nullptr;
  representation = nullptr;
  VALIDATE(pxm->FindProxy(representationId) == nullptr);
  VALIDATE(pxm->FindProxy(shrinkId) == nullptr);
  return true;
}

int TestPipelineBuilderMicroserviceWithRendering(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestPipelineBuilderMicroserviceWithRendering application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  auto* options = app->GetOptions();

  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    if (id == 0)
    {
      app->Exit(1);
    }
    else
    {
      const bool success =
        DoPipelineBuilderMicroserviceTestWithRendering(argc, argv, app->GetSession(id), runLoop);
      app->Await(
        runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  });
  return app->Run(runLoop);
}
