/*=========================================================================

Program:   ParaView
Module:    TestPipelineBuilderMicroservice.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkPipelineBuilderMicroservice.
 */
#include "vtkClientSession.h"
#include "vtkDataObject.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"
#include "vtkSmartPointer.h"
#include "vtkTestUtilities.h"

#include <cstdlib>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

namespace Globals
{
std::string filename;
};

bool DoPipelineBuilderMicroserviceTest(
  vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{

  int errorsCaught = 0;

  vtkNew<vtkPipelineBuilderMicroservice> pipelineBuilder;
  auto app = vtkPVApplication::GetInstance();

  // if session is not set an error is raised
  pipelineBuilder->CreateSource("sources", "SphereSource")
    .subscribe([](vtkSMProxy* /*newProxy*/) { VALIDATE(false); },
      [&errorsCaught](rxcpp::util::error_ptr ep) {
        errorsCaught++;
        vtkLogF(INFO, "%s", rxcpp::util::what(ep).c_str());
      });

  pipelineBuilder->SetSession(session);

  // if the proxy does not exist an error is raised
  pipelineBuilder->CreateSource("sources", "123SphereSource123")
    .subscribe([](vtkSMProxy* /*newProxy*/) { VALIDATE(false); },
      [&errorsCaught](rxcpp::util::error_ptr ep) {
        errorsCaught++;
        vtkLogF(INFO, "%s", rxcpp::util::what(ep).c_str());
      });

  // Create source proxy
  vtkSmartPointer<vtkSMSourceProxy> source = nullptr;
  source = vtk::TakeSmartPointer(
    app->Await(runLoop, pipelineBuilder->CreateSource("sources", "SphereSource")));
  VALIDATE(source != nullptr);

  // Create a filter
  vtkSmartPointer<vtkSMSourceProxy> filter = nullptr;
  bool done = false;
  pipelineBuilder->CreateFilter("filters", "ShrinkFilter", source)
    .subscribe(
      [&filter, &done](vtkSMSourceProxy* newProxy) {
        VALIDATE(newProxy);
        filter = vtk::TakeSmartPointer(newProxy);
        done = true;
      },
      [&errorsCaught](rxcpp::util::error_ptr ep) {
        errorsCaught++;
        vtkLogF(ERROR, "%s", rxcpp::util::what(ep).c_str());
      }

    );

  app->ProcessEventsUntil(runLoop, [&done]() { return done; });
  VALIDATE(filter != nullptr);

  vtkSmartPointer<vtkSMSourceProxy> reader = nullptr;

  // Create a reader
  done = false;
  pipelineBuilder->CreateReader("sources", "IOSSReader", Globals::filename)
    .subscribe(
      [&reader, &done](vtkSMSourceProxy* newProxy) {
        VALIDATE(newProxy);
        reader = vtk::TakeSmartPointer(newProxy);
        done = true;
      },
      [&errorsCaught](rxcpp::util::error_ptr ep) {
        errorsCaught++;
        vtkLogF(ERROR, "%s", rxcpp::util::what(ep).c_str());
      }

    );

  app->ProcessEventsUntil(runLoop, [&done]() { return done; });

  VALIDATE(reader != nullptr);

  // Attempt to delete a source with depedencies
  vtkTypeUInt32 id = source->GetGlobalID();
  done = false;
  bool status = app->Await(runLoop, pipelineBuilder->DeleteProxy(source));
  VALIDATE(status == false);

  auto proxyManager = session->GetProxyManager();
  VALIDATE(proxyManager->FindProxy(id) != nullptr);

  // Delete a proxy with no depedencies
  id = filter->GetGlobalID();

  status = app->Await(runLoop, pipelineBuilder->DeleteProxy(filter));
  VALIDATE(status);
  filter = nullptr;
  VALIDATE(proxyManager->FindProxy(id) == nullptr);

  VALIDATE(errorsCaught == 2);

  return true;
}

int TestPipelineBuilderMicroservice(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestPipelineBuilderMicroservice application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());
  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  if (options->GetDataDirectory().empty())
  {
    char* name = vtkTestUtilities::ExpandDataFileName(argc, argv, "Testing/Data/can.ex2");
    Globals::filename = name;
    delete[] name;
  }
  else
  {
    Globals::filename = options->GetDataDirectory() + "/Testing/Data/can.ex2";
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    if (id == 0)
    {
      app->Exit(1);
    }
    else
    {
      const bool success = DoPipelineBuilderMicroserviceTest(app->GetSession(id), runLoop);
      app->Await(
        runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  });
  return app->Run(runLoop);
}
