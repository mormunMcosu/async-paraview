/*=========================================================================

Program:   ParaView
Module:    TestActiveObjectsMicroservice.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkActiveObjects
 * Validates that the two observables of vtkActiveObjects are emitting the expected results as
 * new proxies are created and selected.
 */
#include "vtkActiveObjectsMicroservice.h"
#include "vtkClientSession.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkSMProxy.h"
#include "vtkSMProxySelectionModel.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"
#include "vtkSmartPointer.h"
#include "vtkTestUtilities.h"

#include <cstdlib>
#include <vector>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

bool DoActiveObjectsMicroserviceTest(
  vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{

  int errorsCaught = 0;

  vtkNew<vtkPipelineBuilderMicroservice> pipelineBuilder;
  vtkNew<vtkActiveObjectsMicroservice> activeObjects;
  auto app = vtkPVApplication::GetInstance();

  pipelineBuilder->SetSession(session);
  activeObjects->SetSession(session);
  activeObjects->SetModelName("ActiveSources");

  // record active objects for validation
  std::vector<std::string> activeObjectsResults;

  rxcpp::composite_subscription currentSubscription =
    activeObjects->GetCurrentObservable().subscribe(
      [&activeObjectsResults](vtkSmartPointer<vtkSMProxy> proxy) {
        if (proxy)
        {
          activeObjectsResults.push_back(proxy->GetXMLName());
        }
        else
        {
          activeObjectsResults.push_back("nullptr");
        }
      });

  // record selections for validation
  std::vector<std::string> selectionResults;
  rxcpp::composite_subscription selectionSubscription =
    activeObjects->GetSelectionObservable().subscribe(
      [&selectionResults](const std::vector<vtkSmartPointer<vtkSMProxy>>& selection) {
        if (selection.empty())
        {
          selectionResults.push_back("<empty>");
        }
        else
        {
          std::string selectionString;
          for (auto proxy : selection)
          {
            selectionString.append(proxy->GetXMLName());
            selectionString.append(",");
          }
          selectionString.pop_back(); // remove last ","
          selectionResults.emplace_back(selectionString);
        }
      });

  // creating a proxy updates "ActiveSources" CurrentProxy
  auto source = vtk::TakeSmartPointer(
    app->Await(runLoop, pipelineBuilder->CreateSource("sources", "SphereSource")));
  VALIDATE(source != nullptr);

  // this should NOT emit any value since selection remained the same
  activeObjects->Select(source, vtkSMProxySelectionModel::SELECT);

  vtkSmartPointer<vtkSMSourceProxy> filter = nullptr;
  bool done = false;
  auto subscription = pipelineBuilder->CreateFilter("filters", "ShrinkFilter", source)
                        .subscribe(
                          [&filter, &done](vtkSMSourceProxy* newProxy) {
                            VALIDATE(newProxy);
                            filter = vtk::TakeSmartPointer(newProxy);
                            done = true;
                          },
                          [&errorsCaught](rxcpp::util::error_ptr ep) {
                            ++errorsCaught;
                            vtkLogF(ERROR, "%s", rxcpp::util::what(ep).c_str());
                          }

                        );

  app->ProcessEventsUntil(runLoop, [&done]() { return done; });
  subscription.unsubscribe();
  VALIDATE(filter != nullptr);
  // add source to selection
  activeObjects->Select(source, vtkSMProxySelectionModel::SELECT);

  // deleting the active source should set current active source to nullptr and selection to empty;
  bool status = app->Await(runLoop, pipelineBuilder->DeleteProxy(filter));
  VALIDATE(status);

  // set active source manually
  activeObjects->SetCurrentProxy(source, vtkSMProxySelectionModel::CLEAR);

  VALIDATE(errorsCaught == 0);

  currentSubscription.unsubscribe();
  selectionSubscription.unsubscribe();

  const std::vector<std::string> expectedActiveObjects = { "nullptr", "SphereSource",
    "ShrinkFilter", "nullptr", "SphereSource" };

  const std::vector<std::string> expectedSelections = { "<empty>", "SphereSource", "ShrinkFilter",
    "ShrinkFilter,SphereSource", "<empty>" };

  VALIDATE(activeObjectsResults == expectedActiveObjects);
  VALIDATE(selectionResults == expectedSelections);

  return true;
}

int TestActiveObjectsMicroservice(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestActiveObjectsMicroservice application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    if (id == 0)
    {
      app->Exit(1);
    }
    else
    {
      const bool success = DoActiveObjectsMicroserviceTest(app->GetSession(id), runLoop);
      app->Await(
        runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  });
  return app->Run(runLoop);
}
