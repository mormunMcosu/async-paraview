/*=========================================================================

Program:   ParaView
Module:    TestFileSystemMicroservice.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkFileSystemMicroservice.
 */

#include "vtkClientSession.h"
#include "vtkCollection.h"
#include "vtkDistributedEnvironment.h"
#include "vtkFileSystemMicroservice.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkTestUtilities.h"
#include "vtkrxcpp/rx-util.hpp"

#include <vtksys/SystemTools.hxx>

#include <cstdlib>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

#define VTK_EXPECT(x, n)                                                                           \
  if (x != n)                                                                                      \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x " == " #n ")...failed!");                                   \
    throw std::runtime_error("Test failed");                                                       \
  }

// LABEL created directories to avoid races between client-server and
// builtin-session test cases when run in parallel
std::string FILESYSTEM_LABEL;
bool DoFileSystemMicroserviceTest(vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop,
  vtkFileSystemMicroservice::Location location)
{
  vtkNew<vtkFileSystemMicroservice> fservice;
  auto app = vtkPVApplication::GetInstance();

  const std::string baseDirectory = vtksys::SystemTools::GetCurrentWorkingDirectory();
  // Split path into its constituent components so we can reuse it to compose testing paths
  std::vector<std::string> directoryPathComponents;
  vtksys::SystemTools::SplitPath(baseDirectory, directoryPathComponents);
  vtkLogF(INFO, "%s", baseDirectory.c_str());

  // if session is not set an error is raised
  int errorsCaught = 0;
  fservice->ListDirectory(baseDirectory)
    .subscribe(
      // on_next
      [](vtkSmartPointer<vtkPVFileInformation> /*fileInfo*/) { VALIDATE(false); },
      // on_error
      [&errorsCaught](rxcpp::util::error_ptr ep) {
        errorsCaught++;
        vtkLogF(INFO, "%s", rxcpp::util::what(ep).c_str());
      });

  fservice->SetSession(session);

  // Test listing of a directory
  // TODO validate the result as part of the test

  vtkSmartPointer<vtkPVFileInformation> fileInfo =
    app->Await(runLoop, fservice->ListDirectory(baseDirectory));
  VALIDATE(fileInfo->GetContents()->GetNumberOfItems() > 0);

  std::vector<std::string> oldPath = directoryPathComponents;
  oldPath.push_back("TestDir_" + FILESYSTEM_LABEL);
  const std::string oldDirectory = vtksys::SystemTools::JoinPath(oldPath);

  bool makeDirStatus = app->Await(runLoop, fservice->MakeDirectory(oldDirectory, location));
  VALIDATE(makeDirStatus);
  VALIDATE(vtksys::SystemTools::FileExists(oldDirectory, /* isFile */ false));

  std::vector<std::string> newPath = directoryPathComponents;
  newPath.push_back("TestDir_" + FILESYSTEM_LABEL + "_new");
  const std::string newDirectory = vtksys::SystemTools::JoinPath(newPath);

  bool renameDirStatus =
    app->Await(runLoop, fservice->Rename(oldDirectory, newDirectory, location));
  VALIDATE(renameDirStatus == true);
  VALIDATE(!vtksys::SystemTools::FileExists(oldDirectory, /* isFile */ false));
  VALIDATE(vtksys::SystemTools::FileExists(newDirectory, /* isFile */ false));

  // deleting a directory that does not exist returns an error
  fservice->Remove(oldDirectory, location)
    .subscribe([](bool /*deleteDirStatus*/) { VALIDATE(false); },
      [&errorsCaught](std::exception_ptr error_ptr) {
        vtkLogF(INFO, "%s", rxcpp::util::what(error_ptr).c_str());
        errorsCaught++;
      });

  std::vector<std::string> newFilePath = newPath;
  newFilePath.push_back("test_remove");

  // create a dummy files to test Remove
  std::string dummyFilePath = vtksys::SystemTools::JoinPath(newFilePath);

  vtksys::Status status = vtksys::SystemTools::Touch(dummyFilePath, /* create */ true);
  VALIDATE(status.IsSuccess());

  std::vector<std::string> newFilePath2 = newPath;
  newFilePath2.push_back("test_remove2");

  dummyFilePath = vtksys::SystemTools::JoinPath(newFilePath2);
  status = vtksys::SystemTools::Touch(dummyFilePath, /* create */ true);
  VALIDATE(status.IsSuccess());

  // remove the file
  VALIDATE(app->Await(runLoop, fservice->Remove(dummyFilePath, location)));
  VALIDATE(!vtksys::SystemTools::FileExists(newDirectory, /* isFile */ true));

  // removing not existent file raises an error
  fservice->Remove(dummyFilePath, location)
    .subscribe([](bool /* status */) { VALIDATE(false); },
      [&errorsCaught](std::exception_ptr error_ptr) {
        vtkLogF(INFO, "%s", rxcpp::util::what(error_ptr).c_str());
        errorsCaught++;
      });

  // Remove a non empty directory
  VALIDATE(app->Await(runLoop, fservice->Remove(newDirectory, location)));
  VALIDATE(!vtksys::SystemTools::FileExists(newDirectory, /* isFile */ false));

  // Create and remove an empty directory
  VALIDATE(app->Await(runLoop, fservice->MakeDirectory(oldDirectory, location)));
  VALIDATE(vtksys::SystemTools::FileExists(oldDirectory, /* isFile */ false));

  // Remove the testing directory
  bool done = false;
  fservice->Remove(oldDirectory, location).subscribe([&done](bool deleteDirStatus) {
    VALIDATE(deleteDirStatus == true);
    done = true;
  });

  app->ProcessEventsUntil(runLoop, [&done]() { return done; });
  VALIDATE(!vtksys::SystemTools::FileExists(oldDirectory, /* isFile */ false));
  VTK_EXPECT(errorsCaught, 3);
  return true;
}

int TestFileSystemMicroservice(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestRemoteFileService application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());
  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
    FILESYSTEM_LABEL = "_builtin_";
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
    FILESYSTEM_LABEL = "_client-server_";
  }

  sessionIdObservable.subscribe(
    [&](vtkTypeUInt32 id) {
      if (id == 0)
      {
        app->Exit(1);
      }
      else
      {
        bool success = DoFileSystemMicroserviceTest(
          app->GetSession(id), runLoop, vtkFileSystemMicroservice::Location::DATA_SERVICE);
        success &= DoFileSystemMicroserviceTest(
          app->GetSession(id), runLoop, vtkFileSystemMicroservice::Location::RENDER_SERVICE);
        success &= DoFileSystemMicroserviceTest(
          app->GetSession(id), runLoop, vtkFileSystemMicroservice::Location::CLIENT);
        app->Await(
          runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
        app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
      }
    },
    // on_error
    [&app](std::exception_ptr error_ptr) {
      vtkLogF(ERROR, "%s", rxcpp::util::what(error_ptr).c_str());
      app->Exit(1);
    });
  return app->Run(runLoop);
}
