/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkDataFileMicroservice.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDataFileMicroservice.h"
#include "vtkClientSession.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkRemoteFileSystemProvider.h"
#include "vtkRemoteObjectProvider.h"
#include "vtkRemotingCoreUtilities.h"
#include "vtkSMParaViewPipelineController.h"
#include "vtkSMProxy.h"
#include "vtkSMReaderFactory.h"
#include "vtkSMSessionProxyManager.h"
#include "vtksys/SystemTools.hxx"

#include "vtk_fmt.h"
// clang-format off
#include VTK_FMT(fmt/core.h)
// clang-format on

namespace
{
/**
 * Returns dirname of file if use_dir is true, else returns the filename.
 */
std::string GetPath(const std::string& filename, bool use_dir)
{
  if (use_dir)
  {
    return vtksys::SystemTools::GetFilenamePath(filename);
  }
  return filename;
}

}

class vtkDataFileMicroservice::vtkInternals
{
public:
  const std::thread::id OwnerTID{ std::this_thread::get_id() };
  vtkSmartPointer<vtkClientSession> Session = { nullptr };

  using ProxyInfo = std::pair<std::string, std::string>;

  //-----------------------------------------------------------------------------
  bool IsInitialized() const { return Session != nullptr; }

  //-----------------------------------------------------------------------------
  std::vector<ProxyInfo> GetPossibleReaders(const std::string& filename) const
  {
    auto readerFactory = this->Session->GetReaderFactory();
    auto results = readerFactory->FindReadersForFile(filename);
    auto resultsForDir = readerFactory->FindReadersForDirectory(filename);
    results.insert(results.end(), resultsForDir.begin(), resultsForDir.end());
    return results;
  }

  //-----------------------------------------------------------------------------
  rxcpp::observable<bool> FileExists(const std::string& filename) const
  {
    // special message that asks the service to check if file exists
    auto packet = vtkRemoteFileSystemProvider::FileExists(filename);
    return this->Session->SendRequest(vtkClientSession::DATA_SERVER, packet)
      .map([](const vtkPacket& reply) { return reply.GetJSON().at("status").get<bool>(); })
      .take(1);
  }

  //-----------------------------------------------------------------------------
  /**
   * Emits 1 if file can be read, 0 if file cannot be read, -1 if reader cannot determine file
   * readability.
   */
  rxcpp::observable<int> CanReadFile(const std::string& filename, const ProxyInfo& info) const
  {
    auto pxm = this->Session->GetProxyManager();
    // create a new reader but do not register it with a ParaViewController
    auto proxy = pxm->NewProxy(info.first.c_str(), info.second.c_str());
    // special message that asks the actual VTK reader if it can read a file. this is light-weight
    auto packet = vtkRemoteObjectProvider::CanReadFile(proxy->GetGlobalID(), filename);
    // needed so that reader is created on remote service.
    proxy->UpdateVTKObjects();

    return this->Session->SendRequest(vtkClientSession::DATA_SERVER, packet)
      .map([proxy](const vtkPacket& reply) {
        // prevent leaked proxy.
        proxy->Delete();
        return reply.GetJSON().at("status").get<int>();
      })
      .take(1);
  }
};

//=============================================================================

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkDataFileMicroservice);

//-----------------------------------------------------------------------------
vtkDataFileMicroservice::vtkDataFileMicroservice()
  : Internals(new vtkDataFileMicroservice::vtkInternals())
{
}

//-----------------------------------------------------------------------------
vtkDataFileMicroservice::~vtkDataFileMicroservice() = default;

//-----------------------------------------------------------------------------
void vtkDataFileMicroservice::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Initialized: " << (this->Internals->IsInitialized() ? "yes" : "no") << "\n";
}

//-----------------------------------------------------------------------------
void vtkDataFileMicroservice::SetSession(vtkClientSession* session)
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  if (internals.Session != session)
  {
    // TODO error handling
    vtkNew<vtkSMParaViewPipelineController> controller;
    controller->InitializeSession(session);
    internals.Session = session;
  }
}

//-----------------------------------------------------------------------------
vtkClientSession* vtkDataFileMicroservice::GetSession() const
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  return internals.Session;
}

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSmartPointer<vtkStringArray>> vtkDataFileMicroservice::FindPossibleReaders(
  const std::string& filename)
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<vtkSmartPointer<vtkStringArray>>(
      std::runtime_error("Session was not set!"));
  }

  auto readerObservable = rxcpp::observable<>::create<vtkSmartPointer<vtkStringArray>>(
    [internals, filename](const rxcpp::subscriber<vtkSmartPointer<vtkStringArray>>& subscriber) {
      // returns take(1). no need to keep track of rxcpp subscription
      internals.FileExists(filename).subscribe(
        [filename, internals, subscriber](const bool exists) -> void {
          auto readers = vtk::TakeSmartPointer(vtkStringArray::New());
          if (!exists)
          {
            vtkLogF(ERROR, "The file \'%s\' does not exist!", filename.c_str());
            subscriber.on_next(readers);
            return;
          }
          auto results = internals.GetPossibleReaders(filename);
          if (results.empty())
          {
            vtkLogF(ERROR, "No suitable reader was found for file \'%s\'", filename.c_str());
            subscriber.on_next(readers);
            return;
          }
          // Stores all readers who may be able to read the file in the form of "group,name"
          std::vector<rxcpp::observable<vtkInternals::ProxyInfo>> infoObservables;
          for (const auto& info : results)
          {
            const auto infoObservable =
              internals.CanReadFile(filename, info)
                .filter([](const int& couldRead) { return couldRead != 0; })
                .map([info](const int&) -> vtkInternals::ProxyInfo { return info; });
            infoObservables.emplace_back(infoObservable);
          }
          // transforms a vector of string observables into a single emission of a vtkStringArray
          auto values = rxcpp::observable<>::iterate(infoObservables);
          values.merge().subscribe(
            // on_next accumulates reader infos
            [readers](const vtkInternals::ProxyInfo& info) {
              readers->InsertNextValue(info.first + ',' + info.second);
            },
            // on_completed emits a complete array of reader info strings
            [readers, subscriber]() { subscriber.on_next(readers); });
        });
    });
  return readerObservable.take(1);
}

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSmartPointer<vtkStringArray>> vtkDataFileMicroservice::FindPossibleReaders(
  const std::vector<std::string>& filegroup)
{
  return this->FindPossibleReaders(filegroup[0]);
}

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSMSourceProxy*> vtkDataFileMicroservice::Open(const std::string& filename)
{
  std::vector<std::string> filegroup({ filename });
  return this->Open(filegroup);
}

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSMSourceProxy*> vtkDataFileMicroservice::Open(
  const std::vector<std::string>& filegroup)
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error("Session was not set!"));
  }

  auto possibleReaders = this->FindPossibleReaders(filegroup);
  return possibleReaders
    .map([session = internals.Session, filegroup](
           const vtkSmartPointer<vtkStringArray>& readers) -> rxcpp::observable<vtkSMSourceProxy*> {
      if (readers->GetNumberOfValues() > 0)
      {
        const auto xmlInfo = readers->GetValue(0);
        const auto pieces = vtksys::SystemTools::SplitString(xmlInfo, ',');
        vtkNew<vtkPipelineBuilderMicroservice> builder;
        builder->SetSession(session);
        return builder->CreateReader(pieces[0], pieces[1], filegroup);
      }
      return rxcpp::observable<>::just<vtkSMSourceProxy*>(nullptr);
    })
    .switch_on_next()
    .take(1);
}
