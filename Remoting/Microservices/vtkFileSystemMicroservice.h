/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkFileSystemMicroservice.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkFileSystemMicroservice
 * @brief A class manipulate the filesystem of a service.
 *
 * vtkFileSystemMicroservice can be used to create/delete/list directories on a service.
 */

#ifndef vtkFileSystemMicroservice_h
#define vtkFileSystemMicroservice_h

#include "vtkObject.h"
#include "vtkPVFileInformation.h"
#include "vtkPythonObservableWrapper.h"     // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkRemotingMicroservicesModule.h" // for exports
#include "vtkSmartPointer.h"                // for vtkSmartPointer

#include <memory> // for std::unique_ptr

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkClientSession;
class vtkPVFileInformation;

class VTKREMOTINGMICROSERVICES_EXPORT vtkFileSystemMicroservice : public vtkObject
{
public:
  static vtkFileSystemMicroservice* New();
  vtkTypeMacro(vtkFileSystemMicroservice, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  enum class Location
  {
    CLIENT,
    DATA_SERVICE,
    RENDER_SERVICE,
    SERVER = DATA_SERVICE,
  };

  ///@{
  /**
   * Returns an observable of the status of the operation
   * This is a single value observable.
   *
   * @section Triggers Triggers
   *
   * * on_next: once the operation completes
   *
   * * on_error: if session is not set, std::runtime_error is thrown
   *
   * * on_completed: right after on_next since this is a single value observable
   */

  rxcpp::observable<bool> MakeDirectory(
    const std::string& absolutePath, Location location = Location::DATA_SERVICE) const;
  rxcpp::observable<bool> Remove(
    const std::string& absolutePath, Location location = Location::DATA_SERVICE) const;
  rxcpp::observable<bool> Rename(const std::string& oldAbsolutePath,
    const std::string& newAbsolutePath, Location location = Location::DATA_SERVICE) const;
  ///@}

  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(bool,
    MakeDirectory(const std::string& absolutePath, Location location = Location::DATA_SERVICE)
      const);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(bool,
    Remove(const std::string& absolutePath, Location location = Location::DATA_SERVICE) const);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(bool,
    Rename(const std::string& oldAbsolutePath, const std::string& newAbsolutePath,
      Location location = Location::DATA_SERVICE) const);

  /**
   * Returns an observable of a vtkPVFileInformation object holding the contents
   * of the directory.
   * This is a single value observable.
   *
   * @section Triggers Triggers
   *
   * * on_next: once the operation completes
   *
   * * on_error: if session is not set, std::runtime_error is thrown
   *
   * * on_completed: right after on_next since this is a single value observable
   */
  rxcpp::observable<vtkSmartPointer<vtkPVFileInformation>> ListDirectory(
    const std::string& absolutePath, Location location = Location::DATA_SERVICE) const;

  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(vtkSmartPointer<vtkObject>,
    ListDirectory(const std::string& absolutePath, Location location = Location::DATA_SERVICE)
      const);

  ///@{
  /**
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

protected:
  vtkFileSystemMicroservice();
  ~vtkFileSystemMicroservice() override;

private:
  vtkFileSystemMicroservice(const vtkFileSystemMicroservice&) = delete;
  void operator=(const vtkFileSystemMicroservice&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
