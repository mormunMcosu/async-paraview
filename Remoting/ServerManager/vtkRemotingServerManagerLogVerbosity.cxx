/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingServerManagerLogVerbosity.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkRemotingServerManagerLogVerbosity.h"
#include "vtksys/SystemTools.hxx"

namespace
{
// executed when this library is loaded.
vtkLogger::Verbosity GetInitialRemotingServerManagerVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::RemotingServerManager module.
  const char* VerbosityKey = "VTKREMOTINGSERVERMANAGER_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    const auto verbosity = vtkLogger::ConvertToVerbosity(verbosity_str);
    if (verbosity > vtkLogger::VERBOSITY_INVALID)
    {
      return verbosity;
    }
  }
  return vtkLogger::VERBOSITY_TRACE;
}

thread_local vtkLogger::Verbosity RemotingServerManagerVerbosity =
  GetInitialRemotingServerManagerVerbosity();
}

//----------------------------------------------------------------------------
vtkLogger::Verbosity vtkRemotingServerManagerLogVerbosity::GetVerbosity()
{
  return ::RemotingServerManagerVerbosity;
}

//----------------------------------------------------------------------------
void vtkRemotingServerManagerLogVerbosity::SetVerbosity(vtkLogger::Verbosity verbosity)
{
  if (verbosity > vtkLogger::VERBOSITY_INVALID)
  {
    ::RemotingServerManagerVerbosity = verbosity;
  }
}
