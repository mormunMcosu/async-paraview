/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingServerManagerLogVerbosity.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkRemotingServerManagerLogVerbosity_h
#define vtkRemotingServerManagerLogVerbosity_h

#include "vtkLogger.h"
#include "vtkRemotingServerManagerModule.h"

/**
 * Looks up system environment for `VTKREMOTINGSERVERMANAGER__LOGGER_VERBOSITY` that shall
 * be used to set logger verbosity for the `vtkRemotingServerManagerModule`.
 * The default value is TRACE.
 *
 * Accepted string values are OFF, ERROR, WARNING, INFO, TRACE, MAX, INVALID or ASCII
 * representation for an integer in the range [-9,9].
 *
 * @note This class internally uses vtkLogger::ConvertToVerbosity(const char*).
 */
class VTKREMOTINGSERVERMANAGER_EXPORT vtkRemotingServerManagerLogVerbosity
{
public:
  /**
   * Get the current log verbosity for the RemotingServerManager module.
   */
  static vtkLogger::Verbosity GetVerbosity();
  static void SetVerbosity(vtkLogger::Verbosity verbosity);
};

/**
 * Macro to use for verbosity when logging ParaView::RemotingServerManager messages. Same as calling
 * vtkRemotingServerManagerLogVerbosity::GetVerbosity() e.g.
 *
 * @code{cpp}
 *  vtkVLogF(VTKREMOTINGSERVERMANAGER_LOG_VERBOSITY(), "a message");
 * @endcode
 */
#define VTKREMOTINGSERVERMANAGER_LOG_VERBOSITY()                                                   \
  vtkRemotingServerManagerLogVerbosity::GetVerbosity()

#endif // vtkRemotingServerManagerLogVerbosity_h
