/*=========================================================================

  Program:   ParaView
  Module:    vtkPVApplicationOptions.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkPVApplicationOptions
 * @brief options subclass for client specific options.
 *
 */

#ifndef vtkPVApplicationOptions_h
#define vtkPVApplicationOptions_h

#include "vtkPVCoreApplicationOptions.h"
#include "vtkRemotingServerManagerModule.h" // for exports

#include <vector> // for std::vector

class VTKREMOTINGSERVERMANAGER_EXPORT vtkPVApplicationOptions : public vtkPVCoreApplicationOptions
{
public:
  static vtkPVApplicationOptions* New();
  vtkTypeMacro(vtkPVApplicationOptions, vtkPVCoreApplicationOptions);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Provides a list of server configurations files to
   * use instead of the default user-specific server configurations file.
   */
  const std::vector<std::string>& GetServerConfigurationsFiles() const;
  void SetServerConfigurationsFiles(const std::vector<std::string>& files);
  ///@}

  ///@{
  /**
   * The server connection url to use to connect to the server process
   * on startup, if any.
   *
   * @sa GetServerResourceName
   */
  const std::string& GetServerURL() const;
  void SetServerURL(const std::string& url);
  ///@}

  ///@{
  /**
   * The server connection resource name to
   * use to connect to the server process(es) on startup, if any.
   */
  const std::string& GetServerResourceName() const;
  void SetServerResourceName(const std::string& name);
  ///@}

  ///@{
  /**
   * The baseline directory to use for image regression tests.
   */
  const std::string& GetBaselineDirectory() const;
  void SetBaselineDirectory(const std::string& name);
  ///@}

  ///@{
  /**
   * The temporary directory to use for generating temporary files or test
   * results.
   */
  const std::string& GetTestDirectory() const;
  void SetTestDirectory(const std::string& name);
  ///@}

  ///@{
  /**
   * The data directory to use for generating temporary files or test
   * results.
   */
  const std::string& GetDataDirectory() const;
  void SetDataDirectory(const std::string& name);
  ///@}

  ///@{
  /**
   * Determines whether the remote server should shutdown
   * when a client disconnects from server.
   */
  const bool& GetServerExit() const;
  void SetServerExit(const bool& value);
  ///@}

  void Populate(CLI::App& app) override;

protected:
  vtkPVApplicationOptions();
  ~vtkPVApplicationOptions() override;

private:
  vtkPVApplicationOptions(const vtkPVApplicationOptions&) = delete;
  void operator=(const vtkPVApplicationOptions&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
