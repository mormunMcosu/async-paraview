/*=========================================================================

  Program:   ParaView
  Module:    vtkRemoteObjectProviderViews.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkRemoteObjectProviderViews.h"

#include "vtkCamera.h"
#include "vtkChannelSubscription.h"
#include "vtkObjectFactory.h"
#include "vtkObjectStore.h"
#include "vtkObjectWrapper.h"
#include "vtkPVDataDeliveryManager.h"
#include "vtkPVDataRepresentation.h"
#include "vtkPVImageDelivery.h"
#include "vtkPVLogger.h"
#include "vtkPVRenderView.h"
#include "vtkPVView.h"
#include "vtkReactiveCommand.h"
#include "vtkRemoteViewResultItem.h"
#include "vtkRemoteViewStatsItem.h"
#include "vtkRemotingServerManagerCoreLogVerbosity.h"
#include "vtkService.h"
#include "vtkServicesCoreLogVerbosity.h"
#include "vtkSmartPointer.h"
#include "vtkTimerLog.h"

#include <cassert>
#include <chrono>
#include <stdexcept>

namespace
{
void FromJSON(vtkCamera* camera, const vtkNJson& json)
{
  double viewUp[3];
  double position[3];
  double focalPoint[3];
  double viewAngle;

  VTK_NJSON_LOAD_MEMBER_ARRAY(json, viewUp);
  VTK_NJSON_LOAD_MEMBER_ARRAY(json, position);
  VTK_NJSON_LOAD_MEMBER_ARRAY(json, focalPoint);
  VTK_NJSON_LOAD_MEMBER(json, viewAngle);

  camera->SetViewUp(viewUp);
  camera->SetPosition(position);
  camera->SetFocalPoint(focalPoint);
  camera->SetViewAngle(viewAngle);
}

vtkNJson ToJSON(vtkCamera* camera)
{
  double viewUp[3];
  camera->GetViewUp(viewUp);

  double position[3];
  camera->GetPosition(position);

  double focalPoint[3];
  camera->GetFocalPoint(focalPoint);

  double viewAngle = camera->GetViewAngle();

  vtkNJson json;
  VTK_NJSON_SAVE_MEMBER_ARRAY(json, viewUp);
  VTK_NJSON_SAVE_MEMBER_ARRAY(json, position);
  VTK_NJSON_SAVE_MEMBER_ARRAY(json, focalPoint);
  VTK_NJSON_SAVE_MEMBER(json, viewAngle);
  return json;
}
} // end of namespace {}

class vtkRemoteObjectProviderViews::vtkInternals
{
public:
  vtkSmartPointer<vtkChannelSubscription> DataChannelSubscription;
  rxcpp::composite_subscription DataChannelRxSubscription;
  bool DataProcessing{ false };
  std::map<vtkTypeUInt32, vtkTimeStamp> PushTimeStamps;
  uint64_t RenderTimeStamp{ 0 };
};

vtkObjectFactoryNewMacro(vtkRemoteObjectProviderViews);
//----------------------------------------------------------------------------
vtkRemoteObjectProviderViews::vtkRemoteObjectProviderViews()
  : Internals(new vtkRemoteObjectProviderViews::vtkInternals())
{
}

//----------------------------------------------------------------------------
vtkRemoteObjectProviderViews::~vtkRemoteObjectProviderViews() = default;

//----------------------------------------------------------------------------
void vtkRemoteObjectProviderViews::HandleObjectStoreEvent(
  vtkObject* vtkNotUsed(caller), unsigned long eventId, void* callData)
{
  // this is called on service's main thread.
  auto& internals = (*this->Internals);
  if (eventId != vtkObjectStore::RegisterObjectEvent)
  {
    return;
  }
  auto* wrapper = vtkObjectWrapper::SafeDownCast(reinterpret_cast<vtkObject*>(callData));
  if (auto* view = wrapper ? vtkPVView::SafeDownCast(wrapper->GetVTKObject()) : nullptr)
  {
    if (internals.DataProcessing)
    {
      vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "Initialize for data processing (%s)",
        vtkLogIdentifier(view));
      view->AddObserver(vtkCommand::UpdateEvent, this, &vtkRemoteObjectProviderViews::PushViewData);
    }
    else
    {
      vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "Initialize for rendering (%s)",
        vtkLogIdentifier(view));
      view->AddObserver(
        vtkCommand::RenderEvent, this, &vtkRemoteObjectProviderViews::HandleViewResult);

      // progress event
      rxvtk::from_event(view, vtkCommand::ProgressEvent)
        .map([view](const auto& /*tuple */) {
          vtkProgressItem item;
          item.Message = "";
          item.Progress = static_cast<int8_t>(view->GetProgress() * 10) * 10;
          item.GlobalID = view->GetGlobalID();
          return item;
        })
        .distinct_until_changed()
        .subscribe(this->GetProgressSubject().get_subscriber());
    }

    internals.PushTimeStamps[view->GetGlobalID()].Modified();
  }
}

//----------------------------------------------------------------------------
void vtkRemoteObjectProviderViews::PushViewData(
  vtkObject* caller, unsigned long eventId, void* vtkNotUsed(callData))
{
  auto& internals = (*this->Internals);
  assert("Only supported on data-processing services" && internals.DataProcessing);
  if (eventId != vtkCommand::UpdateEvent)
  {
    vtkLog(WARNING, "PushViewData only responds to vtkCommand::UpdateEvent") return;
  }

  auto* view = vtkPVView::SafeDownCast(caller);
  auto& tstamp = internals.PushTimeStamps.at(view->GetGlobalID());

  auto* dmanager = view->GetDataDeliveryManager();
  auto dataMap = dmanager->GetData(tstamp);
  tstamp.Modified();

  if (!dataMap.empty())
  {
    vtkVLogScopeF(
      VTKREMOTINGSERVERMANAGERCORE_DATAMOVEMENT_LOG_VERBOSITY(), "Pushing rendering data");
    vtkNJson json;
    json["gid"] = view->GetGlobalID();
    this->GetService()->Publish(
      vtkRemoteObjectProviderViews::GetDataChannelName(), vtkPacket{ std::move(json), dataMap });
  }
}

//----------------------------------------------------------------------------
void vtkRemoteObjectProviderViews::HandleViewResult(
  vtkObject* caller, unsigned long eventId, void* callData)
{
  auto& internals = (*this->Internals);
  assert("Only supported on rendering services" && !internals.DataProcessing);

  auto* view = vtkPVView::SafeDownCast(caller);
  if (eventId != vtkCommand::RenderEvent)
  {
    return;
  }
  if (callData == nullptr)
  {
    vtkLogF(
      ERROR, "Expected to find type vtkPVView::RenderModes in callData, but callData is null");
    throw std::runtime_error("unexpected error occurred");
  }

  const int renderMode = *(reinterpret_cast<int*>(callData));

  vtkRemoteViewStatsItem stats;
  if (auto* renderView = vtkPVRenderView::SafeDownCast(view))
  {
    stats.RayTracing = renderView->IsRayTracingEnabled();
    stats.NumberOfRenderPasses = renderView->GetNumberOfProgressivePasses();
  }
  if (internals.RenderTimeStamp > 0)
  {
    stats.Timestamp = internals.RenderTimeStamp;
  }
  stats.GlobalID = view->GetGlobalID();

  vtkVLogScopeF(APV_LOG_RENDERING_VERBOSITY(), "Publish view content");
  auto deliverer = view->GetImageDeliverer();
  if (deliverer == nullptr)
  {
    vtkLogF(ERROR,
      "View (gid=%d) does not have an image delivery instance. Was the subproxy initialized?",
      view->GetGlobalID());
    throw std::runtime_error("unexpected error occurred");
  }
  bool preferLowQualityPackage = renderMode == vtkPVView::INTERACTIVE;
  vtkPacket packet =
    deliverer->PackageDisplayFramebuffer(view->GetRenderWindow(), stats, preferLowQualityPackage);
  if (!packet.empty())
  {
    this->GetService()->Publish(vtkRemoteObjectProviderViews::GetImageChannelName(), packet);
  }
}

//----------------------------------------------------------------------------
void vtkRemoteObjectProviderViews::InitializeInternal(vtkService* service)
{
  this->Superclass::InitializeInternal(service);

  auto store = this->GetObjectStore();
  store->AddObserver(vtkObjectStore::RegisterObjectEvent, this,
    &vtkRemoteObjectProviderViews::HandleObjectStoreEvent);
  store->AddObserver(vtkObjectStore::UnregisterObjectEvent, this,
    &vtkRemoteObjectProviderViews::HandleObjectStoreEvent);

  auto& internals = (*this->Internals);
  internals.DataProcessing = (service->GetName() == "ds"); // TODO: need a better way
  if (!internals.DataProcessing)
  {
    internals.DataChannelSubscription =
      service->Subscribe(vtkRemoteObjectProviderViews::GetDataChannelName(), "ds");
    internals.DataChannelRxSubscription =
      internals.DataChannelSubscription->GetObservable().subscribe(
        [&internals, this](const vtkPacket& data) {
          vtkVLogScopeF(
            VTKREMOTINGSERVERMANAGERCORE_DATAMOVEMENT_LOG_VERBOSITY(), "Received rendering data");
          const auto& json = data.GetJSON();
          const auto gid = json.at("gid").get<vtkTypeUInt32>();
          auto view = this->GetObjectStore()->FindVTKObject<vtkPVView>(gid);
          if (view)
          {
            auto* dmanager = view->GetDataDeliveryManager();
            dmanager->SetData(data.GetPayload());
          }
        });
  }
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteObjectProviderViews::PiggybackInformation(vtkObject* object) const
{
  if (auto* rvview = vtkPVRenderView::SafeDownCast(object))
  {
    auto& internals = (*this->Internals);
    if (!internals.DataProcessing)
    {
      auto bbox = rvview->GetVisiblePropBounds();
      std::vector<double> bounds(6, 0.0);
      bbox.GetBounds(bounds.data());
      vtkNJson json;
      json["VisiblePropBounds"] = bounds;
      return json;
    }
  }

  return this->Superclass::PiggybackInformation(object);
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteObjectProviderViews::Render(
  vtkTypeUInt32 gid, vtkCamera* camera, bool interactive)
{
  vtkNJson json = vtkNJson::object();
  json["type"] = "vtk-remote-object-render";
  json["gid"] = gid;
  json["camera"] = ::ToJSON(camera);
  json["interactive"] = interactive;
  // add timestamp for each request so we can use to compute frame rate.
  json["timestamp"] = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  return { json };
}

//----------------------------------------------------------------------------
void vtkRemoteObjectProviderViews::Preview(const vtkPacket& packet)
{
  const auto& json = packet.GetJSON();
  const auto type = json.at("type").get<std::string>();
  if (type == "vtk-remote-object-render")
  {
    const auto gid = json.at("gid").get<vtkTypeUInt32>();
    auto view = this->GetObjectStore()->FindVTKObject<vtkPVView>(gid);
    if (view)
    {
      view->DoPreviewRender();
    }
  }
  else
  {
    this->Superclass::Preview(packet);
  }
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteObjectProviderViews::Process(const vtkPacket& packet)
{
  const auto& json = packet.GetJSON();
  const auto type = json.at("type").get<std::string>();
  if (type == "vtk-remote-object-render")
  {
    vtkTypeUInt32 gid = json.at("gid").get<vtkTypeUInt32>();
    auto view = this->GetObjectStore()->FindVTKObject<vtkPVView>(gid);
    if (view)
    {
      if (auto* camera = view->GetActiveCamera())
      {
        ::FromJSON(camera, json.at("camera"));
      }

      auto& internals = (*this->Internals);
      internals.RenderTimeStamp = json.at("timestamp").get<long>();
      const auto& interactive = json["interactive"].get<bool>();
      view->DoRender(interactive);
    }

    return {};
  }
  else if (type == "vtk-remote-object-stop-process-command")
  {
    auto& internals = (*this->Internals);
    if (!internals.DataProcessing)
    {
      // stop listenting to data representations from data channel.
      internals.DataChannelRxSubscription.unsubscribe();
    }
    return this->Superclass::Process(packet);
  }
  else
  {
    return this->Superclass::Process(packet);
  }
}

//----------------------------------------------------------------------------
void vtkRemoteObjectProviderViews::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
