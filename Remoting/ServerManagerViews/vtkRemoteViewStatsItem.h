/*=========================================================================

Program:   ParaView
Module:    vtkRemoteViewStatsItem.h

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

   This software is distributed WITHOUT ANY WARRANTY; without even
   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
   PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @struct vtkRemoteViewStatsItem
 * @brief encapsulates a remote view's metrics and statistics.
 *
 */

#ifndef vtkRemoteViewStatsItem_h
#define vtkRemoteViewStatsItem_h

#include "vtkNJson.h"

struct vtkRemoteViewStatsItem
{
  bool RayTracing = 0;
  int NumberOfRenderPasses = 0;
  int Framerate = 0;
  long Timestamp = 0;
  long Latency = 0;
  long EncodeTime = 0;
  long DisplayGrabTime = 0;
  long GPU2CPUXferTime = 0;
  long RenderPayloadSizeBytes = 0;
  vtkTypeUInt32 GlobalID = 0;

  bool operator==(const vtkRemoteViewStatsItem& other) const
  {
    return this->GlobalID == other.GlobalID && this->RayTracing == other.RayTracing &&
      this->NumberOfRenderPasses == other.NumberOfRenderPasses &&
      this->Framerate == other.Framerate && this->Timestamp == other.Timestamp &&
      this->Latency == other.Latency && this->EncodeTime == other.EncodeTime &&
      this->DisplayGrabTime == other.DisplayGrabTime &&
      this->GPU2CPUXferTime == other.GPU2CPUXferTime &&
      this->RenderPayloadSizeBytes == other.RenderPayloadSizeBytes;
  }

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(vtkRemoteViewStatsItem, GlobalID, RayTracing, NumberOfRenderPasses,
    Framerate, Timestamp, Latency, EncodeTime, DisplayGrabTime, GPU2CPUXferTime,
    RenderPayloadSizeBytes);

  void FromJson(const vtkNJson& json) { from_json(json, *this); }

  vtkNJson ToJson() const
  {
    auto json = vtkNJson::object();
    to_json(json, *this);
    return json;
  }
};

#endif

//  VTK-HeaderTest-Exclude: vtkRemoteViewStatsItem.h
