/*=========================================================================

  Program:   ParaView
  Module:    TestBasicRenderView.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkClientSession.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkNew.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPVDataInformation.h"
#include "vtkRegressionTestImage.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSMInputProperty.h"
#include "vtkSMOutputPort.h"
#include "vtkSMParaViewPipelineControllerWithRendering.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxyProperty.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMViewProxy.h"
#include "vtkSmartPointer.h"

#include <chrono>
#include <cstdlib>
#include <vtk_cli11.h>

bool DoTest(int argc, char** argv, vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{
  auto* pxm = session->GetProxyManager();
  auto controller = vtk::TakeSmartPointer(vtkSMParaViewPipelineControllerWithRendering::New());
  controller->InitializeSession(session);

  auto proxy = vtk::TakeSmartPointer(pxm->NewProxy("sources", "SphereSource"));
  vtkSMPropertyHelper(proxy, "PhiResolution").Set(80);
  vtkSMPropertyHelper(proxy, "ThetaResolution").Set(80);
  controller->PreInitializeProxy(proxy);
  controller->PostInitializeProxy(proxy);
  controller->RegisterPipelineProxy(proxy);
  proxy->UpdateVTKObjects();

  auto shrink =
    vtk::TakeSmartPointer(vtkSMSourceProxy::SafeDownCast(pxm->NewProxy("filters", "ShrinkFilter")));
  vtkSMPropertyHelper(shrink, "Input").Set(proxy, 0);
  controller->PreInitializeProxy(shrink);
  controller->PostInitializeProxy(shrink);
  controller->RegisterPipelineProxy(shrink);
  shrink->UpdateVTKObjects();

  proxy->UpdateInformation().subscribe(
    [](bool changed) { vtkLogF(INFO, "proxy properties have been updated (%d)", changed); });
  shrink->UpdatePipeline(0.0);

  auto viewProxy =
    vtk::TakeSmartPointer(vtkSMViewProxy::SafeDownCast(pxm->NewProxy("views", "RenderView")));
  controller->PreInitializeProxy(viewProxy);
  controller->PostInitializeProxy(viewProxy);
  controller->RegisterViewProxy(viewProxy);
  controller->AssignViewToLayout(viewProxy);
  viewProxy->UpdateVTKObjects();

  auto repr = controller->Show(shrink, 0, viewProxy, "GeometryRepresentation");

  vtkNew<vtkRenderWindowInteractor> iren;
  iren->SetRenderWindow(viewProxy->GetRenderWindow());
  iren->Initialize();

  bool done = false;
  viewProxy->Update().subscribe([viewProxy, &done](bool status) {
    viewProxy->ResetCameraUsingVisiblePropBounds();
    done = true;
  });
  auto app = vtkPVApplication::GetInstance();
  app->ProcessEventsUntil(runLoop, [&done]() { return done; });

  int retVal = vtkRegressionTestImage(viewProxy->GetRenderWindow());

  if (retVal == vtkTesting::DO_INTERACTOR)
  {
    while (!iren->GetDone())
    {
      iren->ProcessEvents();
      app->ProcessEvents(runLoop);
    }
  }
  viewProxy = nullptr;
  return true;
}

int TestBasicRenderView(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("Test View/Representation application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (app->GetRank() == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  auto* options = app->GetOptions();
  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    bool success = DoTest(argc, argv, app->GetSession(id), runLoop);
    app->Await(runLoop, app->GetSession(id)->Disconnect());
    app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
  });
  return app->Run(runLoop);
}
