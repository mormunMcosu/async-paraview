/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingServerManagerViewsLogVerbosity.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkRemotingServerManagerViewsLogVerbosity_h
#define vtkRemotingServerManagerViewsLogVerbosity_h

#include "vtkLogger.h"
#include "vtkRemotingServerManagerViewsModule.h"

/**
 * Looks up system environment for `VTKREMOTINGSERVERMANAGER_VIEWS_LOGGER_VERBOSITY` that shall
 * be used to set logger verbosity for the `vtkRemotingServerManagerViewsModule`.
 * The default value is TRACE.
 *
 * Accepted string values are OFF, ERROR, WARNING, INFO, TRACE, MAX, INVALID or ASCII
 * representation for an integer in the range [-9,9].
 *
 * @note This class internally uses vtkLogger::ConvertToVerbosity(const char*).
 */
class VTKREMOTINGSERVERMANAGERVIEWS_EXPORT vtkRemotingServerManagerViewsLogVerbosity
{
public:
  /**
   * Get the current log verbosity for the RemotingServerManagerViews module.
   */
  static vtkLogger::Verbosity GetVerbosity();
  static void SetVerbosity(vtkLogger::Verbosity verbosity);
};

/**
 * Macro to use for verbosity when logging ParaView::RemotingServerManagerViews messages. Same
 * as calling vtkRemotingServerManagerViewsLogVerbosity::GetVerbosity() e.g.
 *
 * @code{cpp}
 *  vtkVLogF(VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY(), "a message");
 * @endcode
 */
#define VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY()                                              \
  vtkRemotingServerManagerViewsLogVerbosity::GetVerbosity()

#endif // vtkRemotingServerManagerViewsLogVerbosity_h
