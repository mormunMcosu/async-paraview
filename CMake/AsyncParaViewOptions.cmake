#========================================================================
# BUILD OPTIONS:
# Options that affect the ParaView build, in general.
# These should begin with `APV_BUILD_`.
#========================================================================

option(APV_BUILD_SHARED_LIBS "Build ParaView with shared libraries" "ON")

option(APV_BUILD_LEGACY_REMOVE "Remove all legacy code completely" "OFF")
mark_as_advanced(APV_BUILD_LEGACY_REMOVE)

option(APV_BUILD_LEGACY_SILENT "Silence all legacy code messages" "OFF")
mark_as_advanced(APV_BUILD_LEGACY_SILENT)

# ASYNC: this is not tested
## Kits bundle multiple modules together into a single library, this
## is used to dramatically reduce the number of generated libraries.
#cmake_dependent_option(APV_BUILD_WITH_KITS "Build ParaView using kits instead of modules." OFF
#  # Static builds don't make sense with kits. Ignore the flag if shared
#  # libraries aren't being built.
#  "APV_BUILD_SHARED_LIBS" OFF)
#mark_as_advanced(APV_BUILD_WITH_KITS)

option(APV_BUILD_WITH_EXTERNAL "Use external copies of third party libraries by default" OFF)
mark_as_advanced(APV_BUILD_WITH_EXTERNAL)

option(APV_BUILD_ALL_MODULES "Build all modules by default" OFF)
mark_as_advanced(APV_BUILD_ALL_MODULES)
set(_vtk_module_reason_WANT_BY_DEFAULT
  "via `APV_BUILD_ALL_MODULES`")

set(APV_BUILD_TESTING "OFF"
  CACHE STRING "Enable testing")
set_property(CACHE APV_BUILD_TESTING
  PROPERTY
    STRINGS "ON;OFF;WANT")

cmake_dependent_option(APV_BUILD_VTK_TESTING "Enable VTK testing" OFF
  "APV_BUILD_TESTING" OFF)

set(APV_BUILD_EDITION "CANONICAL"
  CACHE STRING "Enable ParaView components essential for requested capabilities.")
set_property(CACHE APV_BUILD_EDITION
  PROPERTY
    STRINGS "CORE;RENDERING;CANONICAL")

set(APV_BUILD_CORE OFF)
set(APV_BUILD_CANONICAL OFF)
set(APV_ENABLE_RENDERING OFF)
set(APV_ENABLE_NONESSENTIAL OFF)
set(APV_ENABLE_EXTRAS OFF) # anything in CANONICAL AND NONESSENTIAL that we do not support yet
if (APV_BUILD_EDITION STREQUAL "CANONICAL")
  # modules are explicitly enabled as we add support for them.
  set(APV_BUILD_CANONICAL ON)
  set(APV_ENABLE_RENDERING ON)
  set(APV_ENABLE_NONESSENTIAL ON)
elseif (APV_BUILD_EDITION STREQUAL "CORE")
  # all are OFF.
  set(APV_BUILD_CORE ON)
elseif (APV_BUILD_EDITION STREQUAL "RENDERING")
  set(APV_BUILD_CORE ON)
  set(APV_ENABLE_RENDERING ON)
endif()

# We want to warn users if APV_BUILD_EDITION is changed after first configure since the default
# state of various other settings may not be what user expects.
if (DEFINED _async_paraview_build_edition_cached AND
    NOT _async_paraview_build_edition_cached STREQUAL APV_BUILD_EDITION)
  message(WARNING "Changing `APV_BUILD_EDITION` after first configure will not "
    "setup defaults for others settings correctly e.g. plugins enabled. It is recommended that you start "
    "with a clean build directory and pass the option to CMake using "
    "'-DAPV_BUILD_EDITION:STRING=${APV_BUILD_EDITION}'.")
endif()
set(_async_paraview_build_edition_cached "${APV_BUILD_EDITION}" CACHE INTERNAL "")

if (APV_BUILD_CORE)
  set(VTK_GROUP_ENABLE_APV_CORE "YES" CACHE INTERNAL "")
else()
  set(VTK_GROUP_ENABLE_APV_CORE "DEFAULT" CACHE INTERNAL "")
endif()

if (APV_BUILD_CANONICAL)
  set(VTK_GROUP_ENABLE_APV_CANONICAL "YES" CACHE INTERNAL "")
else()
  set(VTK_GROUP_ENABLE_APV_CANONICAL "NO" CACHE INTERNAL "")
endif()

#========================================================================
# CAPABILITY OPTIONS:
# Options that affect the build capabilities.
# These should begin with `APV_USE_`.
#========================================================================

# XXX(VTK): External VTK is not yet actually supported.
if (FALSE)
option(APV_USE_EXTERNAL_VTK "Use an external VTK." OFF)
mark_as_advanced(APV_USE_EXTERNAL_VTK)
else ()
set(APV_USE_EXTERNAL_VTK OFF)
endif ()

option(APV_USE_MPI "Enable MPI support for parallel computing" OFF)
option(APV_SERIAL_TESTS_USE_MPIEXEC
  "Used on HPC to run serial tests on compute nodes" OFF)
mark_as_advanced(APV_SERIAL_TESTS_USE_MPIEXEC)
option(APV_USE_CUDA "Support CUDA compilation" OFF)
option(APV_USE_VTKM "Enable VTK-m accelerated algorithms" "${APV_ENABLE_NONESSENTIAL}")
if (UNIX AND NOT APPLE)
  option(APV_USE_MEMKIND  "Build support for extended memory" OFF)
endif ()

option(APV_USE_PYTHON "Enable/Disable Python scripting support" "OFF")
if (APV_BUILD_CANONICAL AND APV_USE_PYTHON)
  set(VTK_NO_PYTHON_THREADS OFF)
  set(VTK_PYTHON_FULL_THREADSAFE ON)
endif()

if (APV_ENABLE_RAYTRACING AND VTK_ENABLE_OSPRAY)
  set(async_paraview_use_materialeditor ON)
else ()
  set(async_paraview_use_materialeditor OFF)
endif ()

#========================================================================
# FEATURE OPTIONS:
# Options that toggle features. These should begin with `APV_ENABLE_`.
#========================================================================

option(APV_ENABLE_RAYTRACING "Build ParaView with OSPray and/or OptiX ray-tracing support" "OFF")

option(APV_ENABLE_LIBVPX "Build ParaView with libvpx remoting. Requires nasm assembler." OFF)

#========================================================================
# MISCELLANEOUS OPTIONS:
# Options that are hard to classify. Keep this list minimal.
# These should be advanced by default.
#========================================================================
option(APV_INSTALL_DEVELOPMENT_FILES "Install development files to the install tree" ON)
mark_as_advanced(APV_INSTALL_DEVELOPMENT_FILES)

option(APV_RELOCATABLE_INSTALL "Do not embed hard-coded paths into the install" ON)
mark_as_advanced(APV_RELOCATABLE_INSTALL)


cmake_dependent_option(APV_INITIALIZE_MPI_ON_CLIENT
  "Initialize MPI on client-processes by default. Can be overridden using command line arguments" ON
  "APV_USE_MPI" OFF)
mark_as_advanced(APV_INITIALIZE_MPI_ON_CLIENT)

#========================================================================================
# Build up list of required and rejected modules
#========================================================================================
set(apv_requested_modules)
set(apv_rejected_modules)

#[==[
Conditionally require/reject optional modules

Use this macro to conditionally require (or reject) modules.

~~~
async_paraview_require_module(
  MODULES             <module>...
  [CONDITION          <condition>]
  [EXCLUSIVE]
~~~

The arguments are as follows:

  * `MODULES`: (Required) The list of modules.
  * `CONDITION`: (Defaults to `TRUE`) The condition under which the modules
    specified are added to the requested list.
  * `EXCLUSIVE`: When sepcified, if `CONDITION` is false, the module will be
    added to the rejected modules list.
#]==]
macro (async_paraview_require_module)
  cmake_parse_arguments(pem
    "EXCLUSIVE"
    ""
    "CONDITION;MODULES"
    ${ARGN})

  if (pem_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR
      "Unparsed arguments for `async_paraview_require_module`: "
      "${pem_UNPARSED_ARGUMENTS}")
  endif ()

  if (NOT DEFINED pem_CONDITION)
    set(pem_CONDITION TRUE)
  endif ()

  if (${pem_CONDITION})
    # message("${pem_CONDITION} == TRUE")
    list(APPEND apv_requested_modules ${pem_MODULES})
    foreach (_pem_module IN LISTS _pem_MODULES)
      set("_vtk_module_reason_${_pem_module}"
        "via `${pem_CONDITION}`")
    endforeach ()
  elseif (pem_EXCLUSIVE)
    # message("${pem_CONDITION} == FALSE")
    list(APPEND apv_rejected_modules ${pem_MODULES})
    foreach (_pem_module IN LISTS _pem_MODULES)
      set("_vtk_module_reason_${_pem_module}"
        "via `${pem_CONDITION}`")
    endforeach ()
  endif()

  unset(pem_EXCLUSIVE)
  unset(pem_CONDITION)
  unset(pem_MODULES)
  unset(pem_UNPARSED_ARGUMENTS)
  unset(_pem_module)
endmacro()

# async modules
async_paraview_require_module(
  CONDITION APV_BUILD_CANONICAL
  MODULES   AsyncParaView::ProcessXML
            AsyncParaView::RemotingApplication
            AsyncParaView::RemotingMicroservices
            AsyncParaView::RemotingServerManager
            AsyncParaView::RemotingServerManagerCore
            AsyncParaView::RemotingServerManagerDefinitions
            AsyncParaView::RemotingServerManagerViews
            AsyncParaView::RemotingReflection
            AsyncParaView::GUISupportDecorators
            AsyncParaView::RemotingWrapping
            AsyncParaView::ServicesCore
            AsyncParaView::ServicesBackend
            AsyncParaView::smTestDriver
            AsyncParaView::UIGenerationCore
            AsyncParaView::WrapRemoting
            AsyncParaView::VTKExtensionsAMR
            AsyncParaView::VTKExtensionsFiltersGeneral
            AsyncParaView::VTKExtensionsIOCore
            VTK::IOIOSS
            VTK::IOParallel
            VTK::IOLegacy
            VTK::IOParallelXML
            VTK::IOXML
  )

set(apv_enable_thallium_default ON)
set(apv_enable_asio_default ON)
if(WIN32)
  set(apv_enable_thallium_default OFF)
endif()

option(APV_ENABLE_THALLIUM "Enable Thallium backend" ${apv_enable_thallium_default})
option(APV_ENABLE_ASIO "Enable Asio backend" ${apv_enable_asio_default})

if( (NOT APV_ENABLE_THALLIUM) AND (NOT APV_ENABLE_ASIO))
  message(FATAL_ERROR "Both THALLIUM and ASIO backends are OFF. At least one backend is required")
endif()

if(WIN32 AND APV_ENABLE_THALLIUM)
  message(FATAL_ERROR "THALLIUM backend is only supported on Linux and MacOS")
endif()

async_paraview_require_module(
  CONDITION APV_BUILD_CANONICAL AND APV_USE_PYTHON
  MODULES   AsyncParaView::APVPython
            AsyncParaView::PythonInitializer
            AsyncParaView::RemotingPythonAsyncCore
            AsyncParaView::RemotingServerManagerPython
  )

async_paraview_require_module(
  CONDITION APV_BUILD_CANONICAL AND APV_USE_MPI
  MODULES   ParaView::icet
  )


# ensures that VTK::mpi module is rejected when MPI is not enabled.
async_paraview_require_module(
  CONDITION APV_USE_MPI
  MODULES   VTK::ParallelMPI
            VTK::mpi
  EXCLUSIVE)

# ensures VTK::Python module is rejected when Python is not enabled.
async_paraview_require_module(
  CONDITION APV_USE_PYTHON
  MODULES   VTK::Python
            VTK::PythonInterpreter
            VTK::WebCore
  EXCLUSIVE)

async_paraview_require_module(
  CONDITION APV_USE_VTKM
  MODULES   VTK::AcceleratorsVTKmFilters
  EXCLUSIVE)

async_paraview_require_module(
  CONDITION APV_ENABLE_RAYTRACING AND APV_ENABLE_RENDERING
  MODULES   VTK::RenderingRayTracing
  EXCLUSIVE)

async_paraview_require_module(
  CONDITION APV_ENABLE_LIBVPX
  MODULES   VTKStreaming::libvpx
  EXCLUSIVE)
  
async_paraview_require_module(
  CONDITION APV_ENABLE_RENDERING AND APV_BUILD_CANONICAL AND APV_BUILD_EXTRAS
  MODULES   VTK::FiltersTexture
            VTK::RenderingFreeType)

async_paraview_require_module(
  CONDITION APV_USE_MPI AND APV_USE_PYTHON
  MODULES   VTK::ParallelMPI4Py)

async_paraview_require_module(
  CONDITION APV_USE_MPI AND APV_BUILD_CANONICAL AND APV_BUILD_EXTRAS
  MODULES   VTK::FiltersParallelFlowPaths
            VTK::FiltersParallelGeometry
            VTK::FiltersParallelMPI
            VTK::IOMPIImage)

async_paraview_require_module(
  CONDITION APV_USE_MPI AND APV_BUILD_CANONICAL AND APV_ENABLE_NONESSENTIAL AND APV_ENABLE_EXTRAS
  MODULES  VTK::IOParallelNetCDF)


if (NOT APV_ENABLE_RENDERING)
  # This ensures that we don't ever enable OpenGL
  # modules when APV_ENABLE_RENDERING is OFF.
  set(rendering_modules
    VTK::glew
    VTK::opengl)
  list(APPEND apv_rejected_modules
    ${rendering_modules})
  foreach (rendering_module IN LISTS rendering_modules)
    set("_vtk_module_reason_${rendering_module}"
      "via `APV_ENABLE_RENDERING` (via `APV_BUILD_EDITION=${APV_BUILD_EDITION}`)")
  endforeach ()

  function (_async_paraview_rendering_option_conflict option name)
    if (${option})
      message(FATAL_ERROR
        "Async ParaView is configured without Rendering support (via the "
        "${APV_BUILD_EDITION} edition) which is incompatible with the "
        "request for ${name} support (via the `${option}` configure option)")
    endif ()
  endfunction ()

  _async_paraview_rendering_option_conflict(APV_ENABLE_RAYTRACING raytracing)
endif()

if (apv_requested_modules)
  list(REMOVE_DUPLICATES apv_requested_modules)
endif ()

if (apv_rejected_modules)
  list(REMOVE_DUPLICATES apv_rejected_modules)
endif()
