#[==[.md
# apv-config.cmake

This file is used by CMake when finding AsyncParaView.

The following variables are provided by this module:

  * `AsyncParaView_VERSION`: The full version of ParaView found.
  * `AsyncParaView_VERSION_SIMPLE`: The simple version of ParaView found (eg: 5.9.1).
  * `AsyncParaView_VERSION_MAJOR`: The major version of ParaView found.
  * `AsyncParaView_VERSION_MINOR`: The minor version of ParaView found.
  * `AsyncParaView_VERSION_PATCH`: The patch version of ParaView found.
  * `AsyncParaView_PREFIX_PATH`: Install prefix for ParaView.
  * `APV_USE_MPI`: If ParaView is built with MPI support.
  * `APV_USE_PYTHON`: If ParaView is built with Python support.
  * `APV_PYTHONPATH`: Where ParaView's Python modules live under the
    install prefix. Unset if Python is not available.
  * `AsyncParaView_LIBRARIES`: The list of modules specified by `COMPONENTS` and
    `OPTIONAL_COMPONENTS`. This may be used in `MODULES` arguments in the API
    (e.g., `vtk_module_autoinit`). All modules are also targets and may be
    linked to using `target_link_libraries`.
#]==]

set(${CMAKE_FIND_PACKAGE_NAME}_CMAKE_MODULE_PATH_save "${CMAKE_MODULE_PATH}")
list(INSERT CMAKE_MODULE_PATH 0
  "${CMAKE_CURRENT_LIST_DIR}")

set("${CMAKE_FIND_PACKAGE_NAME}_CMAKE_PREFIX_PATH_save" "${CMAKE_PREFIX_PATH}")
include("${CMAKE_CURRENT_LIST_DIR}/apv-prefix.cmake")
set("${CMAKE_FIND_PACKAGE_NAME}_PREFIX_PATH"
  "${_vtk_module_import_prefix}")
unset(_vtk_module_import_prefix)
list(INSERT CMAKE_PREFIX_PATH 0
  "${${CMAKE_FIND_PACKAGE_NAME}_PREFIX_PATH}")

set("${CMAKE_FIND_PACKAGE_NAME}_VERSION" "@APV_VERSION_FULL@")
set("${CMAKE_FIND_PACKAGE_NAME}_VERSION_SIMPLE" "@APV_VERSION@")
set("${CMAKE_FIND_PACKAGE_NAME}_VERSION_MAJOR" "@APV_VERSION_MAJOR@")
set("${CMAKE_FIND_PACKAGE_NAME}_VERSION_MINOR" "@APV_VERSION_MINOR@")
set("${CMAKE_FIND_PACKAGE_NAME}_VERSION_PATCH" "@APV_VERSION_PATCH@")

unset("${CMAKE_FIND_PACKAGE_NAME}_FOUND")

set(_paraview_use_external_vtk "@APV_USE_EXTERNAL_VTK@")
set(_paraview_find_package_args)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
  list(APPEND _paraview_find_package_args
    QUIET)
endif ()
if (NOT _paraview_use_external_vtk)
  list(APPEND _paraview_find_package_args
    PATHS "${CMAKE_CURRENT_LIST_DIR}/vtk"
    NO_DEFAULT_PATH)
  # Find VTK with just `CommonCore` to get the available targets first. Only
  # required for a vendored VTK since external targets are not checked by CMake
  # later.
  find_package(VTK REQUIRED
    ${_paraview_find_package_args}
    COMPONENTS CommonCore)
endif ()
unset(_paraview_use_external_vtk)

set(APV_USE_MPI "@APV_USE_MPI@")
set(APV_USE_PYTHON "@APV_USE_PYTHON@")

if (APV_USE_PYTHON)
  set(APV_PYTHONPATH "@APV_PYTHON_SITE_PACKAGES_SUFFIX@")
  include("${CMAKE_CURRENT_LIST_DIR}/ParaViewPython-targets.cmake")
  # Unset this for now; these targets will be defined later.
  unset("${CMAKE_FIND_PACKAGE_NAME}_FOUND")
  unset("${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE")
endif ()

include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}-targets.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}-vtk-module-properties.cmake")

include("${CMAKE_CURRENT_LIST_DIR}/paraview-find-package-helpers.cmake" OPTIONAL)
include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}-vtk-module-find-packages.cmake")

include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}Clients-targets.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/${CMAKE_FIND_PACKAGE_NAME}Clients-vtk-module-properties.cmake")

include("${CMAKE_CURRENT_LIST_DIR}/ParaViewTools-targets.cmake" OPTIONAL)

# Gather the list of required VTK components.
set(_paraview_vtk_components_to_request_required)
set(_paraview_vtk_components_to_request_optional)
if (${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS)
  set(_paraview_search_components
    "${${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS}")
else ()
  set(_paraview_search_components
    "@_paraview_all_components@")
endif ()
foreach (_paraview_component IN LISTS "${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS")
  # Ignore non-existent components here.
  if (NOT TARGET "AsyncParaView::${_paraview_component}")
    continue ()
  endif ()

  # Get all module dependencies.
  get_property(_paraview_component_depends
    TARGET "AsyncParaView::${_paraview_component}"
    PROPERTY "INTERFACE_vtk_module_depends")
  get_property(_paraview_component_private_depends
    TARGET "AsyncParaView::${_paraview_component}"
    PROPERTY "INTERFACE_vtk_module_private_depends")
  get_property(_paraview_component_optional_depends
    TARGET "AsyncParaView::${_paraview_component}"
    PROPERTY "INTERFACE_vtk_module_optional_depends")

  # Keep only VTK dependencies.
  set(_paraview_component_vtk_depends
    ${_paraview_component_depends}
    ${_paraview_component_private_depends}
    ${_paraview_component_optional_depends})
  list(FILTER _paraview_component_vtk_depends INCLUDE REGEX "^VTK::")
  string(REPLACE "VTK::" "" _paraview_component_vtk_depends "${_paraview_component_vtk_depends}")
  if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_${_paraview_component})
    list(APPEND _paraview_vtk_components_to_request_required
      ${_paraview_component_vtk_depends})
  else ()
    list(APPEND _paraview_vtk_components_to_request_optional
      ${_paraview_component_vtk_depends})
  endif ()
endforeach ()
unset(_paraview_component)
unset(_paraview_component_depends)
unset(_paraview_component_private_depends)
unset(_paraview_component_optional_depends)
unset(_paraview_component_vtk_depends)

# Remove duplicate component requests.
if (_paraview_vtk_components_to_request_required)
  list(REMOVE_DUPLICATES _paraview_vtk_components_to_request_required)
endif ()
if (_paraview_vtk_components_to_request_optional)
  list(REMOVE_DUPLICATES _paraview_vtk_components_to_request_optional)
endif ()
# Modules which are required should be removed from the optional component
# list.
if (_paraview_vtk_components_to_request_required)
  list(REMOVE_ITEM _paraview_vtk_components_to_request_optional
    ${_paraview_vtk_components_to_request_required})
endif()

# Now find VTK with all of the components we require.
find_package(VTK REQUIRED
  ${_paraview_find_package_args}
  COMPONENTS ${_paraview_vtk_components_to_request_required}
  OPTIONAL_COMPONENTS ${_paraview_vtk_components_to_request_optional})
if (NOT VTK_FOUND)
  set("${CMAKE_FIND_PACKAGE_NAME}_FOUND" 0)
endif ()
unset(_paraview_find_package_args)
unset(_paraview_vtk_components_to_request_required)
unset(_paraview_vtk_components_to_request_optional)

include("${CMAKE_CURRENT_LIST_DIR}/ParaViewClient.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/ParaViewServerManager.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/ParaViewTesting.cmake")
#include("${CMAKE_CURRENT_LIST_DIR}/vtkModuleWrapClientServer.cmake")

if (@APV_USE_PYTHON@) # APV_USE_PYTHON
  include("${CMAKE_CURRENT_LIST_DIR}/paraview.modules-vtk-python-module-properties.cmake")
endif ()

set(_paraview_components_to_check)
foreach (_paraview_component IN LISTS "${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS")
  if (DEFINED "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND")
    # It was already not-found (likely due to `find-package` failures).
  elseif (TARGET "${CMAKE_FIND_PACKAGE_NAME}::${_paraview_component}")
    list(APPEND _paraview_components_to_check
      "${_paraview_component}")
  else ()
    set("${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND" 0)
    list(APPEND "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_NOT_FOUND_MESSAGE"
      "The ${_paraview_component} component is not available.")
  endif ()
endforeach ()
unset(_paraview_component)

set(_paraview_vtk_components)

while (_paraview_components_to_check)
  list(GET _paraview_components_to_check 0 _paraview_component)
  list(REMOVE_AT _paraview_components_to_check 0)
  if (DEFINED "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND")
    # We've already made a determiniation.
    continue ()
  endif ()

  get_property(_paraview_dependencies
    TARGET    "${CMAKE_FIND_PACKAGE_NAME}::${_paraview_component}"
    PROPERTY  "INTERFACE_paraview_module_depends")
  string(REPLACE "${CMAKE_FIND_PACKAGE_NAME}::" "" _paraview_dependencies "${_paraview_dependencies}")
  set(_paraview_all_dependencies_checked TRUE)
  foreach (_paraview_dependency IN LISTS _paraview_dependencies)
    # Handle VTK module dependencies.
    string(FIND "${_paraview_component}" "VTK::" _paraview_vtk_idx)
    if (NOT _paraview_vtk_idx EQUAL -1)
      unset(_paraview_vtk_idx)
      if (NOT TARGET "${_paraview_dependency}")
        set("${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND" 0)
        list(APPEND "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_NOT_FOUND_MESSAGE"
          "Failed to find the ${_paraview_dependency} module.")
      endif ()
      continue ()
    endif ()
    unset(_paraview_vtk_idx)

    if (DEFINED "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_dependency}_FOUND")
      if (NOT ${CMAKE_FIND_PACKAGE_NAME}_${_paraview_dependency}_FOUND)
        set("${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND" 0)
        list(APPEND "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_NOT_FOUND_MESSAGE"
          "Failed to find the ${_paraview_dependency} component.")
      endif ()
    else ()
      # Check its dependencies.
      list(APPEND _paraview_components_to_check
        "${_paraview_dependency}")
      set(_paraview_all_found FALSE)
    endif ()
  endforeach ()
  if (NOT DEFINED "${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND")
    if (_paraview_all_dependencies_checked)
      set("${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND" 1)
    else ()
      list(APPEND _paraview_components_to_check
        "${_paraview_component}")
    endif ()
  endif ()
  unset(_paraview_all_dependencies_checked)
  unset(_paraview_dependency)
  unset(_paraview_dependencies)
endwhile ()
unset(_paraview_component)
unset(_paraview_components_to_check)

set(_paraview_missing_components)
foreach (_paraview_component IN LISTS "${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS")
  if (NOT ${CMAKE_FIND_PACKAGE_NAME}_${_paraview_component}_FOUND AND ${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_${_paraview_component})
    list(APPEND _paraview_missing_components
      "${_paraview_component}")
  endif ()
endforeach ()

if (_paraview_missing_components)
  list(REMOVE_DUPLICATES _paraview_missing_components)
  list(SORT _paraview_missing_components)
  string(REPLACE ";" ", " _paraview_missing_components "${_paraview_missing_components}")
  set("${CMAKE_FIND_PACKAGE_NAME}_FOUND" 0)
  set("${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE"
    "Could not find the ${CMAKE_FIND_PACKAGE_NAME} package with the following required components: ${_paraview_missing_components}.")
endif ()
unset(_paraview_missing_components)

set("${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES")
if (NOT DEFINED "${CMAKE_FIND_PACKAGE_NAME}_FOUND")
  # If nothing went wrong, we've successfully found the package.
  set("${CMAKE_FIND_PACKAGE_NAME}_FOUND" 1)
  # Build the `_LIBRARIES` variable.
  foreach (_paraview_component IN LISTS "${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS")
    list(APPEND "${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES"
      "${CMAKE_FIND_PACKAGE_NAME}::${_paraview_component}")
  endforeach ()
  unset(_paraview_component)
endif ()

set(CMAKE_PREFIX_PATH "${${CMAKE_FIND_PACKAGE_NAME}_CMAKE_PREFIX_PATH_save}")
unset("${CMAKE_FIND_PACKAGE_NAME}_CMAKE_PREFIX_PATH_save")

set(CMAKE_MODULE_PATH "${${CMAKE_FIND_PACKAGE_NAME}_CMAKE_MODULE_PATH_save}")
unset(${CMAKE_FIND_PACKAGE_NAME}_CMAKE_MODULE_PATH_save)
