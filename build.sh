#!/bin/sh
# Usage
# ./build.sh [tag]
# if no tag is provided, the master branch will be built.
#
# This script makes it convenient to build the project on any
# unix machine. These are the exact steps used in the CI as well.
# Simply clone the project from git, update submodules and run
# run the script.

# Go to specific tag if specified.
if [ $# -eq 1 ];
then
  TAG=$1
  if git show-ref --tags ${TAG} --quiet; then
    git checkout ${TAG}
    git submodule sync --recursive
    git submodule update --recursive --init
  else
    echo "tag $TAG doesn't exist or error in command"
    exit 1
  fi
fi

# Get spack
git clone https://github.com/spack/spack.git work/spack
export SPACK_ROOT=$PWD/work/spack
. ./work/spack/share/spack/setup-env.sh

# Install thallium and other mochi dependencies.
git clone https://github.com/mochi-hpc/mochi-spack-packages.git work/mochi-spackages
spack repo add work/mochi-spackages
spack install mochi-thallium@0.10.1 ^libfabric fabrics=tcp,rxm,sockets ^mochi-margo@0.9.10 ^mercury@2.2.0 ^argobots@dce6e727ffc4ca5b3ffc04cb9517c6689be51ec5=main

# Install cmake
.gitlab/ci/cmake.sh

# Install ninja
.gitlab/ci/ninja.sh

# Install nasm compiler, needed for libvpx compilation
.gitlab/ci/nasm.sh

export PATH=$PWD/.gitlab:$PWD/.gitlab/cmake/bin:$PATH

# Configure and build the project.
spack load mochi-thallium
cmake -GNinja \
 -S . \
 -B ./work/build \
 -D CMAKE_BUILD_TYPE=Release \
 -D PARAVIEW_BUILD_TESTING=WANT \
 -D PARAVIEW_USE_PYTHON=ON \
 -D PARAVIEW_USE_QT=OFF \
 -D PARAVIEW_USE_VTKM=OFF

cmake --build ./work/build
