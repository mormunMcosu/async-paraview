import asyncio
from async_paraview.modules.vtkRemotingMicroservices import vtkFileSystemMicroservice
from async_paraview.modules.vtkRemotingServerManager import vtkClientSession
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)
from async_paraview.modules.vtkRemotingServerManagerCore import vtkPVFileInformation


class FileSystem(vtkFileSystemMicroservice):
    """
    A thin Python Wrapper around vtkFileSystemMicroservice
    """

    def __init__(self, session=None):

        if session is not None:
            if not isinstance(session, vtkClientSession):
                raise TypeError(" 'session' argument is not of type vtkClientSession")

            super().SetSession(session)

    async def MakeDirectory(
        self, absolutePath, location=vtkFileSystemMicroservice.Location.DATA_SERVICE
    ):
        """Create a directory at the given path. If an error occurs it will be reported as a RuntimeError"""

        return await vtkPythonObservableWrapperUtilities.GetFuture(
            super().MakeDirectory(absolutePath, location)
        )

    async def Remove(
        self, absolutePath, location=vtkFileSystemMicroservice.Location.DATA_SERVICE
    ):
        """Remove a file or directory at the given path. If an error occurs it will be reported as a RuntimeError"""

        return await vtkPythonObservableWrapperUtilities.GetFuture(
            super().Remove(absolutePath, location)
        )

    async def Rename(
        self, oldPath, newPath, location=vtkFileSystemMicroservice.Location.DATA_SERVICE
    ):
        """Rename a file or directory from and oldPath to a newPath. If an error occurs it will be reported as a RuntimeError"""

        return await vtkPythonObservableWrapperUtilities.GetFuture(
            super().Rename(oldPath, newPath, location)
        )

    async def ListDirectory(
        self, absolutePath, location=vtkFileSystemMicroservice.Location.DATA_SERVICE
    ):

        fileInfo = await vtkPythonObservableWrapperUtilities.GetFuture(
            super().ListDirectory(absolutePath, location)
        )

        def vtkPVFileInformationToDict(fileInfo):
            value = dict()
            value["name"] = fileInfo.GetName()
            value["full_path"] = fileInfo.GetFullPath()
            value["size"] = fileInfo.GetSize()
            value["modification_time"] = fileInfo.GetModificationTimeAsInt()
            typeId = fileInfo.GetType()
            if typeId == vtkPVFileInformation.SINGLE_FILE:
                value["type"] = "File"
            elif vtkPVFileInformation.IsDirectory(typeId):
                value["type"] = "Directory"
            elif vtkPVFileInformation.IsGroup(typeId):
                value["type"] = "Group"
            else:
                value["type"] = "Invalid"

            value["children"] = []
            for i in range(fileInfo.GetContents().GetNumberOfItems()):
                value["children"].append(
                    vtkPVFileInformationToDict(
                        fileInfo.GetContents().GetItemAsObject(i)
                    )
                )
            return value

        result = {
            "name": fileInfo.GetName(),
            "files": [],
            "directories": [],
            "groups": [],
            "full_path": fileInfo.GetFullPath(),
        }
        for i in range(fileInfo.GetContents().GetNumberOfItems()):
            item = vtkPVFileInformationToDict(fileInfo.GetContents().GetItemAsObject(i))
            if item["type"] == "File":
                result["files"].append(item)
            elif item["type"] == "Directory":
                result["directories"].append(item)
            elif item["type"] == "Group":
                result["groups"].append(item)
            else:
                raise RuntimeError("Unrecognized file Type! ", item["type"])

        return result
