import async_paraview.modules.vtkRemotingMicroservices
from async_paraview.modules.vtkRemotingServerManager import vtkClientSession, vtkSMProxy
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)
from typing import Union


class DataFile(
    async_paraview.modules.vtkRemotingMicroservices.vtkDataFileMicroservice
):
    """
    A thin Python Wrapper around vtkDataFileMicroservice.
    """

    def __init__(self, session):

        if not isinstance(session, vtkClientSession):
            raise TypeError(" 'session' argument is not of type vtkClientSession")

        super().SetSession(session)

    async def FindPossibleReaders(self, filename: Union[str, list, tuple]):
        """Given a sequence of filenames or a single filename, find all possible
        readers. The items in the return value can be used to create
        a desired reader with PipelineBuilder.CreateProxy(group, name).

        Args:
            filename (Union[str, list, tuple]): A fileseries or filegroup or a filename
            A fileseries is a single file that lists a series of files.
                Ex: "blow.vtk.series"
            A filegroup is expected to have similarly named files with alphanumeric suffices.
                Ex: ["can.ex.0", "can.ex.1", "can.ex.2"] is a valid file group.

        Returns:
            list[tuple[str, str]]: A list of readers [(proxygroup1, proxyname1), ..]
        """
        first_file = filename
        if isinstance(filename, list) or isinstance(filename, tuple):
            first_file = filename[0]

        readers = await vtkPythonObservableWrapperUtilities.GetFuture(
            super().FindPossibleReaders(first_file)
        )

        items = []
        for i in range(readers.GetNumberOfValues()):
            item = readers.GetValue(i).split(',')
            items.append(tuple(item))
        return items

    async def Open(self, filename: Union[str, list, tuple]):
        """Create a reader that can open the given files.

        Args:
            filename (Union[str, list, tuple]): A fileseries or filegroup or a filename
            A fileseries is a single file that lists a series of files.
                Ex: "blow.vtk.series"
            A filegroup is expected to have similarly named files with alphanumeric suffices.
                Ex: ["can.ex.0", "can.ex.1", "can.ex.2"] is a valid file group.

        Returns:
            vtkSMSourceProxy: a reader proxy
        """
        files = filename
        if isinstance(filename, str):
            files = [filename]

        reader = await vtkPythonObservableWrapperUtilities.GetFuture(
            super().Open(files)
        )
        return reader
