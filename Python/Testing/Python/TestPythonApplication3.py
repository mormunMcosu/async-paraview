import asyncio

from async_paraview.services import ParaT

""" This example demonstrates how to seamlessly switch between
builtin and remote sessions within a single application."""


async def main():
    app = ParaT()
    # Starts a session connected to builtin server
    session = await app.initialize()
    print("Connected to a builtin server")
    print(session)
    await asyncio.sleep(0.5)

    # Switch to remote session.
    server_url = app.get_options().GetServerURL()
    await app.close(session)
    session = await app.initialize(url=server_url)
    print(f"Connected to a remote server {server_url}")
    print(session)
    await asyncio.sleep(0.5)

    # Switch back to builtin session.
    # Quit the server
    await app.close(session, exit_server=True)
    session = await app.initialize()
    print("Connected to a builtin server again")
    print(session)
    await asyncio.sleep(0.5)
    await app.close(session)


asyncio.run(main())
