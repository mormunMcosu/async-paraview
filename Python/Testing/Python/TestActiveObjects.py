import asyncio

from async_paraview.services import ActiveObjects, PipelineBuilder, ParaT

from async_paraview.modules.vtkRemotingServerManager import (
    vtkSMProxySelectionModel,
)  # for vtkSMProxySelectionModel.SELECT

import sys

# anext introduced in 3.10
if sys.version_info.major == 3 and sys.version_info.minor < 10:

    async def anext(awaitable):
        async for value in awaitable:
            return value


def Equal(a, b):
    if a != b:
        print("a : ", a)
        print("b : ", b)
        return False
    return True


async def observeActive(activeObjects):
    observed = []
    async for value in activeObjects.GetCurrentObservable():
        if value:
            observed.append(value.GetXMLName())
        else:
            observed.append(None)

    return observed


async def observeSelection(activeObjects):
    observed = []
    async for value in activeObjects.GetSelectionObservable():
        value = [v.GetXMLName() if v is not None else None for v in value]
        observed.append(value)
    return observed


async def main():
    App = ParaT()
    session = await App.initialize()

    activeObjects = ActiveObjects(session, "ActiveSources")

    task1 = asyncio.create_task(observeActive(activeObjects))
    task2 = asyncio.create_task(observeSelection(activeObjects))

    builder = PipelineBuilder(session)

    # proxy creation updates active and selection
    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )

    # Get current active
    assert await anext(activeObjects.GetCurrentObservable()) == sphere

    shrink = await builder.CreateProxy(
        "filters", "ShrinkFilter", Input=sphere, ShrinkFactor=0.3
    )
    # add manually proxy to the selection
    activeObjects.Select(sphere, vtkSMProxySelectionModel.SELECT)

    # set manually active proxy
    activeObjects.SetCurrentProxy(sphere, vtkSMProxySelectionModel.CLEAR)

    # clear selection
    activeObjects.Select(None, vtkSMProxySelectionModel.CLEAR)
    # select two elements
    activeObjects.Select([sphere, shrink], vtkSMProxySelectionModel.SELECT)

    status = await builder.DeleteProxy(shrink)
    assert status
    status = await builder.DeleteProxy(sphere)
    assert status

    # unsubscribing  terminates the async iterator
    activeObjects.UnsubscribeAll()

    await task1
    await task2
    observedActive = task1.result()
    observedSelection = task2.result()

    assert Equal(
        observedActive,
        [
            None,
            "SphereSource",
            "ShrinkFilter",
            "SphereSource",
            None,
        ],
    )
    assert Equal(
        observedSelection,
        [
            [],
            ["SphereSource"],
            ["ShrinkFilter"],
            ["ShrinkFilter", "SphereSource"],
            [],
            ["SphereSource", "ShrinkFilter"],
            [],
        ],
    )

    #  activeObjects can be reused:
    task1 = asyncio.create_task(observeActive(activeObjects))

    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    activeObjects.UnsubscribeAll()

    await task1
    observedActive = task1.result()
    assert observedActive == [None, "SphereSource"]

    await App.close(session)


asyncio.run(main())
