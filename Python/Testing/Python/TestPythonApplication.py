import asyncio

from async_paraview.services import ParaT

""" This example demonstrates how to initialize and finalize an application in
the async framework. """


async def main():
    app = ParaT()
    session = await app.initialize()
    print(session)
    await asyncio.sleep(0.5)

    await app.close(session)


asyncio.run(main())
