import asyncio

from async_paraview.services import ParaT, DataFile, PropertyManager
from vtkmodules.vtkCommonCore import VTK_UNSTRUCTURED_GRID

from vtk.util.misc import vtkGetDataRoot
import os.path

expectedReaders =  [('sources', 'LegacyVTKFileReader')]

async def main():
    App = ParaT()
    session = await App.initialize()

    datafileService = DataFile(session)
    pm = PropertyManager()

    filename = os.path.join(vtkGetDataRoot(), "Testing/Data/FileSeries/blow.vtk.series")
    possibleReaders = await datafileService.FindPossibleReaders(filename)
    assert possibleReaders == expectedReaders

    reader = await datafileService.Open(filename)
    assert reader is not None

    success = await pm.UpdatePipeline(reader, 0)
    assert success == True
    di = reader.GetDataInformation()

    await App.close(session)

    assert di.GetDataSetType() == VTK_UNSTRUCTURED_GRID
    assert di.GetNumberOfPoints() == 687
    assert di.GetNumberOfCells() == 1057
    assert di.GetNumberOfTimeSteps() == 10


asyncio.run(main())
