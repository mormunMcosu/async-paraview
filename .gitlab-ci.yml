include:
    # Metadata shared by many jobs
    - local: .gitlab/rules.yml
    - local: .gitlab/artifacts.yml

    # OS builds.
    - local: .gitlab/os-linux.yml
    - local: .gitlab/os-macos.yml
    - local: .gitlab/os-windows.yml

stages:
    - prep
    - build
    - test

################################################################################
# Job declarations
#
# Each job must pull in each of the following keys:
#
#   - a "base image"
#   - a build script
#   - tags for the jobs
#   - rules for when to run the job
#
# Additionally, jobs may also contain:
#
#   - artifacts
#   - dependency/needs jobs for required jobs
################################################################################

# Linux

## Prepare, Build and test

fedora35:spack:prep:
    extends:
        - .fedora35
        - .cmake_prep_linux_spack
        - .cmake_prep_artifacts
        - .linux_builder_tags
        - .run_automatically

fedora35:build:
    extends:
        - .fedora35
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_dependent
    dependencies:
        - fedora35:spack:prep
    needs:
        - fedora35:spack:prep

fedora35-thallium:test:
    extends:
        - .fedora35_thallium
        - .cmake_test_linux
        - .cmake_test_artifacts
        - .linux_test_tags
        - .run_dependent
    dependencies:
        - fedora35:build
    needs:
        - fedora35:build

fedora35-asio:test:
    extends:
        - .fedora35_asio
        - .cmake_test_linux
        - .cmake_test_artifacts
        - .linux_test_tags
        - .run_dependent
    dependencies:
        - fedora35:build
    needs:
        - fedora35:build

## Lint builds

fedora35-asan:build:
    extends:
        - .fedora35_asan
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually
    dependencies:
        - fedora35:spack:prep
    needs:
        - fedora35:spack:prep

fedora35-tsan:build:
    extends:
        - .fedora35_tsan
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually
    dependencies:
        - fedora35:spack:prep
    needs:
        - fedora35:spack:prep

fedora35-ubsan:build:
    extends:
        - .fedora35_ubsan
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually
    dependencies:
        - fedora35:spack:prep
    needs:
        - fedora35:spack:prep

## Lint testing
fedora35-asan-asio:test:
    extends:
        - .fedora35_asan_asio
        - .cmake_memcheck_linux
        - .cmake_test_artifacts
        - .linux_test_priv_tags
        - .run_dependent
    dependencies:
        - fedora35-asan:build
    needs:
        - fedora35-asan:build

fedora35-asan-thallium:test:
    extends:
        - .fedora35_asan_thallium
        - .cmake_memcheck_linux
        - .cmake_test_artifacts
        - .linux_test_priv_tags
        - .run_dependent
    dependencies:
        - fedora35-asan:build
    needs:
        - fedora35-asan:build

fedora35-tsan-asio:test:
    extends:
        - .fedora35_tsan_asio
        - .cmake_memcheck_linux
        - .cmake_test_artifacts
        - .linux_test_priv_tags
        - .run_dependent
    dependencies:
        - fedora35-tsan:build
    needs:
        - fedora35-tsan:build

fedora35-tsan-thallium:test:
    extends:
        - .fedora35_tsan_thallium
        - .cmake_memcheck_linux
        - .cmake_test_artifacts
        - .linux_test_priv_tags
        - .run_dependent
    dependencies:
        - fedora35-tsan:build
    needs:
        - fedora35-tsan:build

fedora35-ubsan-asio:test:
    extends:
        - .fedora35_ubsan_asio
        - .cmake_memcheck_linux
        - .cmake_test_artifacts
        - .linux_test_priv_tags
        - .run_dependent
    dependencies:
        - fedora35-ubsan:build
    needs:
        - fedora35-ubsan:build

fedora35-ubsan-thallium:test:
    extends:
        - .fedora35_ubsan_thallium
        - .cmake_memcheck_linux
        - .cmake_test_artifacts
        - .linux_test_priv_tags
        - .run_dependent
    dependencies:
        - fedora35-ubsan:build
    needs:
        - fedora35-ubsan:build

# macOS

## Prepare, Build and test

### macos_arm64
macos-arm64:spack:prep:
    extends:
        - .macos_arm64
        - .cmake_prep_macos_spack
        - .cmake_prep_artifacts
        - .macos_arm64_builder_tags
        - .run_automatically

macos-arm64:build:
    extends:
        - .macos_arm64
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_arm64_builder_tags
        - .run_automatically
    dependencies:
        - macos-arm64:spack:prep
    needs:
        - macos-arm64:spack:prep

macos-arm64-thallium:test:
    extends:
        - .macos_arm64
        - .macos_thallium_addon
        - .cmake_test_macos
        - .cmake_test_artifacts
        - .macos_arm64_builder_tags
        - .run_dependent
    dependencies:
        - macos-arm64:build
    needs:
        - macos-arm64:build

macos-arm64-asio:test:
    extends:
        - .macos_arm64
        - .macos_asio_addon
        - .cmake_test_macos
        - .cmake_test_artifacts
        - .macos_arm64_builder_tags
        - .run_dependent
    dependencies:
        - macos-arm64:build
    needs:
        - macos-arm64:build

### macos_x86_64
macos-x86_64:spack:prep:
    extends:
        - .macos_x86_64
        - .cmake_prep_macos_spack
        - .cmake_prep_artifacts
        - .macos_x86_64_builder_tags
        - .run_automatically

macos-x86_64:build:
    extends:
        - .macos_x86_64
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_x86_64_builder_tags
        - .run_automatically
    dependencies:
        - macos-x86_64:spack:prep
    needs:
        - macos-x86_64:spack:prep

macos-x86_64-thallium:test:
    extends:
        - .macos_x86_64
        - .macos_thallium_addon
        - .cmake_test_macos
        - .cmake_test_artifacts
        - .macos_x86_64_builder_tags
        - .run_dependent
    dependencies:
        - macos-x86_64:build
    needs:
        - macos-x86_64:build

macos-x86_64-asio:test:
    extends:
        - .macos_x86_64
        - .macos_asio_addon
        - .cmake_test_macos
        - .cmake_test_artifacts
        - .macos_x86_64_builder_tags
        - .run_dependent
    dependencies:
        - macos-x86_64:build
    needs:
        - macos-x86_64:build

# Windows

## Build and test

windows-vs2022:build:
    extends:
        - .windows_vs2022
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags
        - .run_automatically
    # no need to wait for `prep` stage
    needs: []
    dependencies: []


windows-vs2022:test:
    extends:
        - .windows_vs2022
        - .cmake_test_windows
        - .cmake_test_artifacts
        - .windows_test_tags
        - .run_dependent
    dependencies:
        - windows-vs2022:build
    needs:
        - windows-vs2022:build
