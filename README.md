# Asynchronous ParaView as a Service

## Introduction

This project aims to provide a processing and visalization library that build upon [ParaView][paraview-master] to support large data processing with or without MPI.
The asynchronous execution of its data-processing and rendering service allow the client application to remain available and enable more responsive and interactive workflows.
While the concepts of ParaView server manager, proxies, properties and domains remain, we've added a new micro-service layer that aims to unify and simplify application development.
Micro-services are aiming to capture application logic in a focus fashion while offering a nice and easy to use API.
Such abstraction will helps in the development of domain-specific solutions by reducing the amount of application code needed.

Thanks to such library, you should be able to create domain-specific responsive visualization applications which do not stall during data processing or rendering in an effortless fashion.

[Reference examples](https://gitlab.kitware.com/async/paraview-async-examples) are also available for getting started.

## Async ParaView Library
The library allows rapid prototyping of ideas and offers a rich API for developer interactions
with `ServerManager`. The application state resides in the
`ServerManager` whereas, application logic no longer sits in a
monolithic core as in [ParaView][paraview-master]; instead, portable and
reusable modules called `Microservices` implement the business logic.
Currently the following microservices are implemented, for example usage see the linked test validating the microservice:
- [`vtkActiveObjectsMicroservice`][ActiveObjectsTest]: Track and update "active" or "selected" proxies.
- [`vtkDataFileMicroservice`][DataFileTest]: Load data files.
- [`vtkDefinitionManagerMicroservice`][DefinitionManagerTest]: Access the definition of
  a proxy. The components of the definition are used to create UI elements
  adapted to the properties of a proxy.
- [`vtkFileSystemMicroservice`][FileSystemTest]: Manipulate the filesystem on the server.
- [`vtkPipelineBuilderMicroservice`][PipelineBuilder]: Helper for managing proxy creation to build visualization pipelines.
- [`vtkPipelineViewerMicroservice`][PipelineViewer]: Get the processing pipeline connectivity so it can easily be displayed.
- `vtkProgressObserverMicroservice`: Listen to progress events from any service.
- [`vtkPropertyManagerMicroservice`][PropertyManager]: Helper for setting and getting properties of any proxy.

[ActiveObjectsTest]: Python/Testing/Python/TestActiveObjects.py
[DataFileTest]: Python/Testing/Python/TestDataFileOpenSingle.py
[DefinitionManagerTest]: Python/Testing/Python/TestDefinitionManager.py
[FileSystemTest]: Python/Testing/Python/TestFileSystem.py
[PipelineBuilder]: Python/Testing/Python/TestPipelineBuilder.py
[PipelineViewer]: Python/Testing/Python/TestPipelineViewer.py
[PropertyManager]: Python/Testing/Python/TestPipelineBuilder.py

This library is designed to support a core feature-set that enable rapid development of any domain-specific web/desktop applications through
[trame][].

[paraview-master]: https://gitlab.kitware.com/paraview/paraview.git
[trame]: https://kitware.github.io/trame/index.html

## Showcases videos

<a href="https://youtu.be/jR46DwR5QLY"><img src="https://img.youtube.com/vi/jR46DwR5QLY/0.jpg"  width="200" /></a> <a href="https://youtu.be/HUjRl6DjF68"><img src="https://img.youtube.com/vi/HUjRl6DjF68/0.jpg"  width="200" /></a>

## High resolution capabilities

High resolution display with image delivery has always been a challenge but
thanks to the streaming capabilities of our rendering service we have options
and by design we are ready for future codecs and capabilities. The table below
just illustrates some of the benefits of using hardware accelerated codecs such as
H.264. For reference, the RGB approach is what the current ParaView Qt client
is using and as you can see we have a major boost here.

| Image Compression | FHD(1920x1080) | UWQHD (3440x1440) | 4k UHD (3840x2160) |
|-------------------|----------------|-------------------|--------------------|
| Lossless - RGB | 22fps @109MB/s | 5.5fps @50MB/s | 3.8fps @65MB/s |
| JPEG           | 30fps @3.3MB/s | 10fps @2.3MB/s | 6fps @2.3MB/s  |
| VP9 (libvpx)   | 30fps @1.1MB/s | 25fps @2.0MB/s | 15fps @1.2MB/s |
| H.264 (nvenc)  | 30fps @1.1MB/s | 29fps @2.0MB/s | 29fps @2.0MB/s |


## Building
The easiest method for beginners to build ParaView from source is
by using our [Getting Started compilation guide][build] which includes
commands to install the needed dependencies for Linux and MacOS.

[build]: Documentation/dev/build.md

## Examples
Visit [paraview-async-examples][] to look at reference examples using trame to create applications using the asynchronous
paraview library.

[paraview-async-examples]: https://gitlab.kitware.com/async/paraview-async-examples.git

## Reporting Bugs
If you find a bug:

1. If you have a source-code fix, please read the [CONTRIBUTING.md][] document.

2. If the issue is not resolved by the above steps, open
   an entry in the [Async ParaView Issue Tracker][].

[CONTRIBUTING.md]: CONTRIBUTING.md
[Async ParaView Issue Tracker]: https://gitlab.kitware.com/async/paraview/-/issues

## Contributing
See [CONTRIBUTING.md][] for instructions to contribute.

[CONTRIBUTING.md]: CONTRIBUTING.md

## License

ParaView Async is distributed under the OSI-approved BSD 3-clause License.
See [Copyright.txt][] for details. For additional licenses, refer to
[ParaView Licenses][].

[Copyright.txt]: Copyright.txt
[ParaView Licenses]: http://www.paraview.org/paraview-license/
