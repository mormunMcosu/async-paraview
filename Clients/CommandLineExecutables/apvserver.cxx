/*=========================================================================

Program:   ParaView
Module:    apvserver.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDistributedEnvironment.h"
#include "vtkNew.h"
#include "vtkPVServerApplication.h"
#include "vtkPVServerOptions.h"

#include <vtk_cli11.h>

//----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  vtkDistributedEnvironment environment(&argc, &argv);

  CLI::App cli(
    "apvserver: the ParaView server\n"
    "=============================\n"
    "This is the ParaView server executable. This is intended for client-server use-cases "
    "which require the client and server to be on different processes, potentially on "
    "different systems.\n\n"
    "Typically, one connects a ParaView client (either a graphical client, or a Python-based "
    "client) to this process to drive the data analysis and visualization pipelines.",
    "apvserver");

  // let's use nicer formatter for help text.
  vtkPVServerOptions::SetupDefaults(cli);

  vtkNew<vtkPVServerApplication> app;

  // populate CLI with ParaView options
  app->GetOptions()->Populate(cli);

  // In contrast to pvpython or testing executables, pvserver has a fixed
  // number of arguments, anything else should be considered an error.
  cli.allow_extras(false);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    app->Exit(app->GetRank() == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  do
  {
    app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());
    app->WaitForExit(runLoop, std::chrono::seconds(app->GetOptions()->GetTimeout()));
    app->Finalize();
  } while (app->GetExitCode() ==
    static_cast<int>(vtkPVServerApplication::PVServerExitCodeEnum::PVSEC_Restart));
  return app->GetExitCode();
}
