# Changelog

<!--next-version-placeholder-->

## v0.9.0 (2022-11-13)
- Add simple build script for linux, macos operating systems
- Remove FFmpeg third party dependency and use libvpx for vp9, nvenc for h264
- Improve image delivery for desktop client
- Fix EGL crash: Always use default EGL index instead of probing command line argument
- Progress observer microservice also reports metrics/stats for a service
- Enable clip filter
- Use interactive/still settings for image delivery with low/high quality compression
- Emit progress events during rendering

## v0.8.0 (2022-10-06)
- Resurrect support for proxy list domains
- ApplyController adds python API for vtkSMApplyController

## v0.7.0 (2022-10-06)
- Introduce progress observer microservice that reports progress for a given service
- PropertyManager: Improve python API for usage with vector elements
- Create a python async iterator out of a vtkCommand event for convenience in python applications
- Definition manager API: `GetCompatibleDifinitions(filter,group)` returns all the filters in `group` that `filter` can accept as input
- Add a prototype trame web-application (SC22)
- Fix crash: Image delivery safely ignores invalid window dimensions like negative integers or 0

## v0.6.0 (2022-10-03)
- Resurrect property inheritance in proxy definitions
- Trame/Simput: Initial steps to integrate with definition manager microservice
- Disable all writer proxy definitions

## v0.5.0 (2022-09-28)
- Introduce pipeline viewer microservice
- PropertyManager: fix GetValues for ProxyProperty

## v0.4.0 (2022-09-27)
- Introduce proxy definition manager microservice
- Make vtkPVGeneralSettings thread safe

## v0.3.1 (2022-09-21)
- Images are optionally delivered with JPEG  compression
- Enhance encode performance with OpenGL video frames
- Improve property names for view proxy
- Remove unnecessary third-party libraries
- Fix render view settings crash by creating it only on the render service

## v0.3.0 (2022-09-06)
- Improve integration with python virtual environment
- Rework python package
- Leverage video codecs to stream encoded video chunks and display them with ffmpeg

## v0.2.1 (2022-08-30)
- Some fixes for compilation on macOS with clang
- Improve memory management in python interface
- Cleanup property manager python API
- vtkPVServerApplication does not use hostname in URL composition
- Reorganize vtkPVApplication options

## v0.2.0 (2022-08-16)
- Introduce microservice that tracks active/selected proxy objects
- Some fixes for cmake: make qt optional, make python optional

## v0.1.0 (2022-08-03)
- Introduce pipeline builder microservice
- Introduce property manager microservice
- Wrap C++ observable to python future

## v0.0.1 (2022-07-22)
- Introduce microservice to browse file system on a service
- Enable volume representation for image data
- Enable ordered compositing
- Enable translucent geometry
- Enable python interface to servermanager
